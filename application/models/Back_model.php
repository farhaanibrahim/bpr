<?php
/**
 *
 */
class Back_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  function getUserInfo($data)
  {
    return $this->db->get_where('t_user',$data);
  }

  function getRekapData()
  {
    return $this->db->get('t_rekap');
  }
  function getNeracaData()
  {
    return $this->db->get('neraca');
  }
  function getLabarugiData()
  {
    return $this->db->get('labarugi');
  }
  function getCAR()
  {
    return $this->db->get('t_car');
  }
  function getKAP()
  {
    return $this->db->get('t_kap');
  }
  function getPPAP()
  {
    return $this->db->get('t_ppap');
  }
  function getManajemen()
  {
    return $this->db->get('t_manajemen');
  }
  function getROA()
  {
    return $this->db->get('t_roa');
  }
  function getBOPO()
  {
    return $this->db->get('t_bopo');
  }
  function get_cr_ldr()
  {
    return $this->db->get('t_cr_ldr');
  }

  public function getRekapDataForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('t_rekap');
  }

  public function getNeracaDataForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('neraca');
  }

  public function getLabarugiDataForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('labarugi');
  }
  public function getCarForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('t_car');
  }

  public function getKapForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('t_kap');
  }
  public function getPpapForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('t_ppap');
  }
  public function getManajemenForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('t_manajemen');
  }
  public function getRoaForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('t_roa');
  }
  public function getBopoForSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('t_bopo');
  }
  public function get_cr_ldr_forSetda()
  {
    $this->db->where('status','1');
    return $this->db->get('t_cr_ldr');
  }

  public function truncate_rekap()
  {
    $this->db->truncate('t_rekap');
  }
  public function truncate_neraca()
  {
    $this->db->truncate('neraca');
  }
  public function truncate_labarugi()
  {
    $this->db->truncate('labarugi');
  }
  public function truncate_car()
  {
    $this->db->truncate('t_car');
  }
  public function truncate_kap()
  {
    $this->db->truncate('t_kap');
  }
  public function truncate_ppap()
  {
    $this->db->truncate('t_ppap');
  }
  public function truncate_manajemen()
  {
    $this->db->truncate('t_manajemen');
  }
  public function truncate_roa()
  {
    $this->db->truncate('t_roa');
  }
  public function truncate_bopo()
  {
    $this->db->truncate('t_bopo');
  }
  public function truncate_cr_ldr()
  {
    $this->db->truncate('t_cr_ldr');
  }

  function insertRekap($data)
  {
    $this->db->insert('t_rekap',$data);
  }
  function insertNeraca($data)
  {
    $this->db->insert('neraca',$data);
  }
  function insertLabarugi($data)
  {
    $this->db->insert('labarugi',$data);
  }
  public function insertCAR($data)
  {
    $this->db->insert('t_car',$data);
  }
  public function insertKAP($data)
  {
    $this->db->insert('t_kap',$data);
  }
  public function insertPPAP($data)
  {
    $this->db->insert('t_ppap',$data);
  }
  public function insertManajemen($data)
  {
    $this->db->insert('t_manajemen',$data);
  }
  public function insertROA($data)
  {
    $this->db->insert('t_roa',$data);
  }
  public function insertBOPO($data)
  {
    $this->db->insert('t_bopo',$data);
  }
  public function insert_cr_ldr($data)
  {
    $this->db->insert('t_cr_ldr',$data);
  }


  function cek_periode($periode,$table)
  {
	$this->db->where('periode',$periode);
	return $this->db->get($table);
  }

  function getAllAccount()
  {
    return $this->db->get('t_user');
  }

  function getAccountById($id_account)
  {
    $this->db->where('id_user',$id_account);
    return $this->db->get('t_user');
  }
  function update_account($id_account,$data)
  {
    $this->db->where('id_user',$id_account);
    $this->db->update('t_user',$data);
  }
  public function instansi()
  {
    $this->db->where('id',1);
    return $this->db->get('tr_instansi');
  }

  public function setting_web($data)
  {
    $this->db->where('id',1);
    $this->db->set($data);
    $this->db->update('tr_instansi');
  }
  public function getInstansi()
  {
    return $this->db->get('tr_instansi');
  }

  public function konfirmasi($periode,$data,$table)
  {
    $this->db->where('periode',$periode);
    $this->db->set($data);
    $this->db->update($table);
  }
  public function send_notif($data)
  {
    $this->db->insert('t_notif',$data);
  }
  public function getNotif()
  {
    $this->db->where('status','unread');
    return $this->db->get('t_notif');
  }
  public function getInbox()
  {
    return $this->db->get('t_notif');
  }
  public function getInboxById($id)
  {
    $this->db->where('id_notif',$id);
    return $this->db->get('t_notif');
  }
  public function update_status_msg($id)
  {
    $this->db->where('id_notif',$id);
    $this->db->set('status','read');
    $this->db->update('t_notif');
  }

  function undo_import($table,$periode)
  {
    $this->db->where('periode',$periode);
    $this->db->delete($table);
  }

  public function getRekapById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('t_rekap');
  }

  public function getNeracaById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('neraca');
  }

  public function getLabarugiById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('labarugi');
  }


  public function getCarById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('t_car');
  }

  public function getKAPById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('t_kap');
  }

  public function getPPAPById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('t_ppap');
  }

  public function getManajemenById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('t_manajemen');
  }
  public function getRoaById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('t_roa');
  }
  public function getBopoById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('t_bopo');
  }
  public function getCrldrById($id)
  {
    $this->db->where('id',$id);
    return $this->db->get('t_cr_ldr');
  }

  public function update_data($data,$id,$table)
  {
    $this->db->where('id',$id);
    $this->db->set($data);
    $this->db->update($table);
  }
  public function tambah_user($data)
  {
    $this->db->insert('t_user',$data);
  }
  public function delete_user($id)
  {
    $this->db->where('id_user',$id);
    $this->db->delete('t_user');
  }

  public function log($data_log)
  {
    $this->db->insert('t_log',$data_log);
  }

  public function getNeracaTriwulan($date)
  {
    $this->db->where('Periode',$date);
    return $this->db->get('neraca');
  }
  public function getNeracaTriwulan2($date)
  {
    $this->db->where('Periode',$date);
    $this->db->where('status',1);
    return $this->db->get('neraca');
  }
  public function getLabarugiTriwulan($date)
  {
    $this->db->where('Periode',$date);
    return $this->db->get('labarugi');
  }
  public function getLabarugiTriwulan2($date)
  {
    $this->db->where('Periode',$date);
    $this->db->where('status',1);
    return $this->db->get('labarugi');
  }

  public function getCarGrafik($year)
  {
    $this->db->where("periode LIKE '".$year."%'");
    $this->db->where("faktor LIKE 'capital'");
    return $this->db->get('t_rekap');
  }
  public function getKapGrafik($year)
  {
    $this->db->where("periode LIKE '".$year."%'");
    $this->db->where("faktor LIKE 'kap'");
    return $this->db->get('t_rekap');
  }
  public function getPpapGrafik($year)
  {
    $this->db->where("periode LIKE '".$year."%'");
    $this->db->where("faktor LIKE 'rasio ppap'");
    return $this->db->get('t_rekap');
  }
  public function getManajemenGrafik($year)
  {
    $this->db->where("periode LIKE '".$year."%'");
    $this->db->where("faktor LIKE 'management'");
    return $this->db->get('t_rekap');
  }
  public function getRoaGrafik($year)
  {
    $this->db->where("periode LIKE '".$year."%'");
    $this->db->where("faktor LIKE 'roa'");
    return $this->db->get('t_rekap');
  }
  public function getBopoGrafik($year)
  {
    $this->db->where("periode LIKE '".$year."%'");
    $this->db->where("faktor LIKE 'bopo'");
    return $this->db->get('t_rekap');
  }
  public function getCrldrGrafik($year)
  {
    $this->db->where("periode LIKE '".$year."%'");
    $this->db->where("faktor LIKE 'cash ratio'");
    return $this->db->get('t_rekap');
  }
  public function getDataNotConfirm()
  {
    $this->db->where('status',0);
    $this->db->get('t_rekap');
    $this->db->get('neraca');
  }
}
