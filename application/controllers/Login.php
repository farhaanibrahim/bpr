<?php
/**
 *
 */
class Login extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Back_model');
  }
  public function index()
  {
    if ($this->session->userdata('status')=='login') {
      redirect(base_url('app'));
    } else {
      if (isset($_POST['btnSubmit'])) {
        $data = array(
          'username'=>$this->input->post('username'),
          'password'=>md5($this->input->post('password'))
        );
        $query = $this->Back_model->getUserInfo($data);
        $query_logo = $this->db->get('tr_instansi');
        foreach($query_logo->result_array() as $logo){$logo = $logo['logo'];}
        if ($query->num_rows() == 1) {
          foreach ($query->result_array() as $key => $value) {
            $newdata = array(
              'id_user'=>$value['id_user'],
              'username'=>$value['username'],
              'nama'=>$value['nama'],
              'nip'=>isset($value['nip']) ? $value['nip'] : "",
              'level'=>$value['level'],
              'logo'=>$logo,
              'status'=>'login'
            );
          }
          $this->session->set_userdata($newdata);
          $data_log = array(
              'id_log' => '',
              'username' => $this->session->userdata('username'),
              'waktu' => date('Y/m/d h:i:s'),
              'activity' => 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
            );
          $this->Back_model->log($data_log);
          redirect(base_url('app'));
        } else {
          redirect(base_url('login'));
        }
      } else {
        $data['instansi'] = $this->Back_model->instansi();
        $this->load->view('login',$data);
      }
    }
  }

  public function logout()
  {
    if ($this->session->userdata('status')=='login') {
      $this->session->sess_destroy();
      redirect(base_url('login'));
    } else {
      redirect(base_url('login'));
    }

  }
}
