<?php
ob_start();
/**
 *
 */
class App extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Back_model');
    $this->load->library('excel');
  }

  function index()
  {
    if ($this->session->userdata('status')=='login') {
      $year = date('Y');
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['jml_rekap'] = $this->Back_model->getRekapData()->num_rows();
      $data['jml_neraca'] = $this->Back_model->getNeracaData()->num_rows();
      $data['jml_labarugi'] = $this->Back_model->getLabarugiData()->num_rows();
      $data['jml_car'] = $this->Back_model->getCAR()->num_rows();

      $data['instansi'] = $this->Back_model->getInstansi();

      $data['tahun1'] = $year;
      $data['tahun2'] = $year-1;
      $data['tahun3'] = $year-2;

      //Data CAR
      $data['car1'] = $this->Back_model->getCarGrafik($year);
      $data['car2'] = $this->Back_model->getCarGrafik($year-1);
      $data['car3'] = $this->Back_model->getCarGrafik($year-2);

      //Data KAP
      $data['kap1'] = $this->Back_model->getKapGrafik($year);  
      $data['kap2'] = $this->Back_model->getKapGrafik($year-1);
      $data['kap3'] = $this->Back_model->getKapGrafik($year-2);

      //Data PPAP
      $data['ppap1'] = $this->Back_model->getPpapGrafik($year);  
      $data['ppap2'] = $this->Back_model->getPpapGrafik($year-1);
      $data['ppap3'] = $this->Back_model->getPpapGrafik($year-2);

      //Data Management
      $data['manajemen1'] = $this->Back_model->getManajemenGrafik($year);  
      $data['manajemen2'] = $this->Back_model->getManajemenGrafik($year-1);
      $data['manajemen3'] = $this->Back_model->getManajemenGrafik($year-2);

      //Data ROA
      $data['roa1'] = $this->Back_model->getRoaGrafik($year);  
      $data['roa2'] = $this->Back_model->getRoaGrafik($year-1);
      $data['roa3'] = $this->Back_model->getRoaGrafik($year-2);

      //Data BOPO
      $data['bopo1'] = $this->Back_model->getBopoGrafik($year);  
      $data['bopo2'] = $this->Back_model->getBopoGrafik($year-1);
      $data['bopo3'] = $this->Back_model->getBopoGrafik($year-2);

      //Data CR_LDR
      $data['cr_ldr1'] = $this->Back_model->getCrldrGrafik($year);  
      $data['cr_ldr2'] = $this->Back_model->getCrldrGrafik($year-1);
      $data['cr_ldr3'] = $this->Back_model->getCrldrGrafik($year-2);
      $this->load->view('home',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  function import_rekap()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_rekap',$data);
    } else {
      redirect(base_url('login'));
    }
  }

  public function rekap_ac()
  {
    if ($this->session->userdata('status')=='login') {
      // config upload
          $config['upload_path'] = './tmp_excel_upload/';
          $config['allowed_types'] = 'xls|xlsx';
          $config['max_size'] = '10000';
          $this->load->library('upload', $config);
		  
		  $cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'t_rekap');
          if($cek_periode->num_rows() > 0)
		  {
			$this->session->set_flashdata('periode_exist','Periode sudah ada');
			redirect(base_url('app/import_rekap'));			
		  } else 
		  {
			//false
			if ( ! $this->upload->do_upload('rekap')) {
						  // jika validasi file gagal, kirim parameter error ke index
						  $error = array('error' => $this->upload->display_errors());
						  $this->index($error);
					  } else {
						  // jika berhasil upload ambil data dan masukkan ke database
						  $upload_data = $this->upload->data();
						  $file_name = $upload_data['file_name'];
						  $extension = $upload_data['file_ext'];

						  if ($extension == '.xlsx') {
							$objReader = PHPExcel_IOFactory::createReader('Excel2007');
						  } else {
							$objReader = PHPExcel_IOFactory::createReader('Excel5');
						  }

						  $objReader->setReadDataOnly(true);

						  //jika kosongkan data dicentang jalankan kode berikut
										$drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
										if($drop == 1){
									  //kosongkan tabel pegawai
												 $this->Back_model->truncate_rekap();
										};

						  $objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
						  $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
						  $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

						  for ($i=8; $i <= $totalrows; $i++) {
							$bobot = $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue()*100 : ' ';
							$data = array(
							  'id'=>'',
							  'no'=>$objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() : ' ',
							  'faktor'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
							  'rasio'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
							  'nkk'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
							  'bobot'=>$bobot.'%',
							  'nkf'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
							  'predikat'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() :  '',
							  'periode'=>$this->input->post('periode'),
							  'tgl'=>date('Ymd'),
							  'status'=>'0'
							);

							$this->Back_model->insertRekap($data);
						  }

						  echo $totalrows;
						  // delete image (for demo purpose only)
						  $this->load->helper('file');
						  delete_files('./tmp_excel_upload/');

						  $this->session->set_flashdata("rekap_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
						  redirect(base_url('app/import_rekap'));
					  }			
		  }
    } else {
      redirect(base_url('login'));
    }

  }

  function import_neraca()
  {
    if ($this->session->userdata('status') == 'login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_neraca',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  function neraca_ac()
  {
    if ($this->session->userdata('status')=='login') {
      // config upload
          $config['upload_path'] = './tmp_excel_upload/';
          $config['allowed_types'] = 'xls|xlsx';
          $config['max_size'] = '10000';
          $this->load->library('upload', $config);
		  
		  $cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'neraca');
			if($cek_periode->num_rows() > 0)
			{
				$this->session->set_flashdata('periode_exist','Periode Sudah ada');
				redirect(base_url('app/import_neraca'));
			} else
			{
				if ( ! $this->upload->do_upload('neraca')) {
					  // jika validasi file gagal, kirim parameter error ke index
					  $error = array('error' => $this->upload->display_errors());
					  $this->index($error);
				  } else {
					  // jika berhasil upload ambil data dan masukkan ke database
					  $upload_data = $this->upload->data();
					  $file_name = $upload_data['file_name'];
					  $extension = $upload_data['file_ext'];

					  if ($extension == '.xlsx') {
						$objReader = PHPExcel_IOFactory::createReader('Excel2007');
					  } else {
						$objReader = PHPExcel_IOFactory::createReader('Excel5');
					  }

					  $objReader->setReadDataOnly(true);

					  //jika kosongkan data dicentang jalankan kode berikut
									$drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
									if($drop == 1){
								  //kosongkan tabel neraca
											 $this->Back_model->truncate_neraca();
									};

					  $objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
					  $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
					  $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

					  for ($i=7; $i <= $totalrows; $i++) {
						$data = array(
						  'id'=>'',
						  'nopos'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
						  'nmpos'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
						  'saldoBK'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
						  'debet'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
						  'kredit'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
						  'saldo'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() :  '',
						  'Periode'=>$this->input->post('periode'),
						  'tgl'=>date('Ymd'),
						  'status'=>'0'
						);

						$this->Back_model->insertNeraca($data);
					  }

					  echo $totalrows;
					  // delete image (for demo purpose only)
					  $this->load->helper('file');
					  delete_files('./tmp_excel_upload/');

					  $this->session->set_flashdata("neraca_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
					  redirect(base_url('app/import_neraca'));
				  }
			}
    } else {
      redirect('login');
    }
  }

  function import_labarugi()
  {
    if ($this->session->userdata('status') == 'login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_labarugi',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  function labarugi_ac()
  {
    if ($this->session->userdata('status')=='login') {
          // config upload
          $config['upload_path'] = './tmp_excel_upload/';
          $config['allowed_types'] = 'xls|xlsx';
          $config['max_size'] = '10000';
          $this->load->library('upload', $config);
		  
		  $cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'labarugi');
		  
		  if($cek_periode->num_rows() > 0)
		  {
			$this->session->set_flashdata('periode_exist','Periode Sudah ada');
			redirect(base_url('app/import_labarugi'));
		  } 
		  else
		  {
			if ( ! $this->upload->do_upload('labarugi')) {
              // jika validasi file gagal, kirim parameter error ke index
              $error = array('error' => $this->upload->display_errors());
              $this->index($error);
			  } else {
				  // jika berhasil upload ambil data dan masukkan ke database
				  $upload_data = $this->upload->data();
				  $file_name = $upload_data['file_name'];
				  $extension = $upload_data['file_ext'];

				  if ($extension == '.xlsx') {
					$objReader = PHPExcel_IOFactory::createReader('Excel2007');
				  } else {
					$objReader = PHPExcel_IOFactory::createReader('Excel5');
				  }

				  $objReader->setReadDataOnly(true);

				  //jika kosongkan data dicentang jalankan kode berikut
								$drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
								if($drop == 1){
							  //kosongkan tabel neraca
										 $this->Back_model->truncate_labarugi();
								};

				  $objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
				  $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
				  $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

				  for ($i=6; $i <= $totalrows; $i++) {
					$data = array(
					  'id'=>'',
					  'nopos'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
					  'nmpos'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
					  'saldoBK'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
					  'debet'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
					  'kredit'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
					  'saldo'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() :  '',
					  'Periode'=>$this->input->post('periode'),
					  'tgl'=>date('Ymd'),
					  'status'=>'0'
					);

					$this->Back_model->insertLabarugi($data);
				  }

				  echo $totalrows;
				  // delete image (for demo purpose only)
				  $this->load->helper('file');
				  delete_files('./tmp_excel_upload/');

				  $this->session->set_flashdata("labarugi_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
				  redirect(base_url('app/import_labarugi'));
			  }	  
		  }	

          
    } else {
      redirect(base_url('login'));
    }
  }

  function rekap()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      if ($this->session->userdata('level')=='setda') {
        $data['rekap'] = $this->Back_model->getRekapDataForSetda();
        $this->load->view('view_rekap2',$data);
      } else {
        $data['rekap'] = $this->Back_model->getRekapData();
        $this->load->view('view_rekap',$data);
      }

    } else {
      redirect(base_url('login'));
    }

  }
  function neraca()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      if ($this->session->userdata('level')=='setda') {
        $data['neraca_setda_view'] = $this->Back_model->getNeracaDataForSetda();
        $this->load->view('view_neraca2',$data);
      } else {
        $data['neraca'] = $this->Back_model->getNeracaData();
        $this->load->view('view_neraca',$data);
      }
    } else {
      redirect(base_url('login'));
    }

  }

  function view_neraca_triwulan()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['neraca'] = $this->Back_model->getNeracaTriwulan($this->input->post('periode'));
      if (substr($this->input->post('periode'), 5,2) == '01') {
        $data['bulan'] = "Januari";
      } elseif (substr($this->input->post('periode'), 5,2) == '02') {
        $data['bulan'] = "Februari";
      } elseif (substr($this->input->post('periode'), 5,2) == '03') {
        $data['bulan'] = "Maret";
      } elseif (substr($this->input->post('periode'), 5,2) == '04') {
        $data['bulan'] = "April";
      } elseif (substr($this->input->post('periode'), 5,2) == '05') {
        $data['bulan'] = "Mei";
      } elseif (substr($this->input->post('periode'), 5,2) == '06') {
        $data['bulan'] = "Juni";
      } elseif (substr($this->input->post('periode'), 5,2) == '07') {
        $data['bulan'] = "Juli";
      } elseif (substr($this->input->post('periode'), 5,2) == '08') {
        $data['bulan'] = "Agustus";
      } elseif (substr($this->input->post('periode'), 5,2) == '09') {
        $data['bulan'] = "September";
      } elseif (substr($this->input->post('periode'), 5,2) == '10') {
        $data['bulan'] = "Oktober";
      } elseif (substr($this->input->post('periode'), 5,2) == '11') {
        $data['bulan'] = "November";
      } else {
        $data['bulan'] = "Desember";
      }
	  $data['tahun'] = substr($this->input->post('periode'), 0,4);
      $this->load->view('view_neraca_triwulan',$data);
    } else {
      redirect(base_url('login'));
    }
    
  }

  function view_neraca_triwulan2()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['neraca'] = $this->Back_model->getNeracaTriwulan2($this->input->post('periode'));
      if (substr($this->input->post('periode'), 5,2) == '03') {
        $data['bulan'] = "Maret";
      } elseif (substr($this->input->post('periode'), 5,2) == '06') {
        $data['bulan'] = "Juni";
      } elseif (substr($this->input->post('periode'), 5,2) == '09') {
        $data['bulan'] = "September";
      } else {
        $data['bulan'] = "Desember";
      }
      $data['tahun'] = substr($this->input->post('periode'), 0,4);
      $this->load->view('view_neraca_triwulan2',$data);
    } else {
      redirect(base_url('login'));
    }
    
  }

  public function neraca_pdf($periode)
  {
    $data['neraca1'] = $this->Back_model->getNeracaTriwulan($periode);
    if (substr($this->input->post('periode'), 5,2) == '03') {
        $data['bulan'] = "Maret";
      } elseif (substr($this->input->post('periode'), 5,2) == '06') {
        $data['bulan'] = "Juni";
      } elseif (substr($this->input->post('periode'), 5,2) == '09') {
        $data['bulan'] = "September";
      } else {
        $data['bulan'] = "Desember";
      }
      $data['tahun'] = substr($periode, 0,4);

      $html = $this->load->view('neraca_pdf',$data,true);

      $pdfFilePath = "neraca.pdf";

      $this->load->library('m_pdf');

      $this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

      $this->m_pdf->pdf->Output($pdfFilePath, "I");
  }

  public function neraca_pdf2($periode)
  {
    $data['neraca1'] = $this->Back_model->getNeracaTriwulan2($periode);
    if (substr($this->input->post('periode'), 5,2) == '03') {
        $data['bulan'] = "Maret";
      } elseif (substr($this->input->post('periode'), 5,2) == '06') {
        $data['bulan'] = "Juni";
      } elseif (substr($this->input->post('periode'), 5,2) == '09') {
        $data['bulan'] = "September";
      } else {
        $data['bulan'] = "Desember";
      }
      $data['tahun'] = substr($periode, 0,4);

    $html = $this->load->view('neraca_pdf',$data,true);

    $pdfFilePath = "neraca.pdf";

    $this->load->library('m_pdf');

    $this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

    $this->m_pdf->pdf->Output($pdfFilePath, "I");
  }

  function labarugi()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      if ($this->session->userdata('level')=='setda') {
        $data['labarugi_setda_view'] = $this->Back_model->getLabarugiDataForSetda();
        $this->load->view('view_labarugi2',$data);
      } else {
        $data['labarugi'] = $this->Back_model->getLabarugiData();
        $this->load->view('view_labarugi',$data);
      }
    } else {
      redirect(base_url('login'));
    }

  }

  function view_labarugi_triwulan()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['labarugi'] = $this->Back_model->getLabarugiTriwulan($this->input->post('periode'));
      if (substr($this->input->post('periode'), 5,2) == '01') {
        $data['bulan'] = "Januari";
      } elseif (substr($this->input->post('periode'), 5,2) == '02') {
        $data['bulan'] = "Februari";
      } elseif (substr($this->input->post('periode'), 5,2) == '03') {
        $data['bulan'] = "Maret";
      } elseif (substr($this->input->post('periode'), 5,2) == '04') {
        $data['bulan'] = "April";
      } elseif (substr($this->input->post('periode'), 5,2) == '05') {
        $data['bulan'] = "Mei";
      } elseif (substr($this->input->post('periode'), 5,2) == '06') {
        $data['bulan'] = "Juni";
      } elseif (substr($this->input->post('periode'), 5,2) == '07') {
        $data['bulan'] = "Juli";
      } elseif (substr($this->input->post('periode'), 5,2) == '08') {
        $data['bulan'] = "Agustus";
      } elseif (substr($this->input->post('periode'), 5,2) == '09') {
        $data['bulan'] = "September";
      } elseif (substr($this->input->post('periode'), 5,2) == '10') {
        $data['bulan'] = "Oktober";
      } elseif (substr($this->input->post('periode'), 5,2) == '11') {
        $data['bulan'] = "November";
      } else {
        $data['bulan'] = "Desember";
      }
      $data['tahun'] = substr($this->input->post('periode'), 0,4);
      $this->load->view('view_labarugi_triwulan',$data);
    } else {
      redirect(base_url('login'));
    }
    
  }
  function view_labarugi_triwulan2()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['labarugi'] = $this->Back_model->getLabarugiTriwulan2($this->input->post('periode'));
      if (substr($this->input->post('periode'), 5,2) == '03') {
        $data['bulan'] = "Maret";
      } elseif (substr($this->input->post('periode'), 5,2) == '06') {
        $data['bulan'] = "Juni";
      } elseif (substr($this->input->post('periode'), 5,2) == '09') {
        $data['bulan'] = "September";
      } else {
        $data['bulan'] = "Desember";
      }
      $data['tahun'] = substr($this->input->post('periode'), 0,4);
      $this->load->view('view_labarugi_triwulan2',$data);
    } else {
      redirect(base_url('login'));
    }
    
  }

  public function labarugi_pdf($periode)
  {
    $data['labarugi1'] = $this->Back_model->getLabarugiTriwulan($periode);
    if (substr($periode, 5,2) == '03') {
        echo $data['bulan'] = "Maret";
      } elseif (substr($this->input->post('periode'), 5,2) == '06') {
        echo $data['bulan'] = "Juni";
      } elseif (substr($this->input->post('periode'), 5,2) == '09') {
        echo $data['bulan'] = "September";
      } else {
        echo $data['bulan'] = "Desember";
      }
      $data['tahun'] = substr($periode, 0,4);

    $html = $this->load->view('labarugi_pdf',$data,true);

    $pdfFilePath = "labarugi.pdf";

    $this->load->library('m_pdf');

    $this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

    $this->m_pdf->pdf->Output($pdfFilePath, "I");
  }
  public function labarugi_pdf2($periode)
  {
    $data['labarugi1'] = $this->Back_model->getLabarugiTriwulan2($periode);
    if (substr($periode, 5,2) == '03') {
        echo $data['bulan'] = "Maret";
      } elseif (substr($this->input->post('periode'), 5,2) == '06') {
        echo $data['bulan'] = "Juni";
      } elseif (substr($this->input->post('periode'), 5,2) == '09') {
        echo $data['bulan'] = "September";
      } else {
        echo $data['bulan'] = "Desember";
      }
      $data['tahun'] = substr($periode, 0,4);

    $html = $this->load->view('labarugi_pdf',$data,true);

    $pdfFilePath = "labarugi.pdf";

    $this->load->library('m_pdf');

    $this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

    $this->m_pdf->pdf->Output($pdfFilePath, "I");
  }

  public function import_car()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_car',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function car_ac()
  {
    if ($this->session->userdata('status')=='login') {
            // config upload
            $config['upload_path'] = './tmp_excel_upload/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);
			
			$cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'t_car');
			if($cek_periode->num_rows() > 0)
			{
				$this->session->set_flashdata('periode_exist','Periode Sudah Ada');
				redirect(base_url('app/import_car'));
			}
			else
			{
				if ( ! $this->upload->do_upload('car')) {
                // jika validasi file gagal, kirim parameter error ke index
                $error = array('error' => $this->upload->display_errors());
                $this->index($error);
				} else {
					// jika berhasil upload ambil data dan masukkan ke database
					$upload_data = $this->upload->data();
					$file_name = $upload_data['file_name'];
					$extension = $upload_data['file_ext'];

					if ($extension == '.xlsx') {
					  $objReader = PHPExcel_IOFactory::createReader('Excel2007');
					} else {
					  $objReader = PHPExcel_IOFactory::createReader('Excel5');
					}

					$objReader->setReadDataOnly(true);

					//jika kosongkan data dicentang jalankan kode berikut
								$drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
								if($drop == 1){
							  //kosongkan tabel neraca
										 $this->Back_model->truncate_car();
								};

					$objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
					$totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
					$objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

					for ($i=7; $i <= $totalrows; $i++) {
					  $data = array(
						'id'=>'',
						'keterangan'=>$objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() : ' ',
						'p_bln_lalu'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
						'p_saat_ini'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
						'periode'=>$this->input->post('periode'),
						'tgl'=>date('Ymd'),
						'status'=>'0'
					  );

					  $this->Back_model->insertCAR($data);
					}

					echo $totalrows;
					// delete image (for demo purpose only)
					$this->load->helper('file');
					delete_files('./tmp_excel_upload/');

					$this->session->set_flashdata("car_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
					redirect(base_url('app/import_car'));
				}
			}
			
            
    } else {
      redirect(base_url('login'));
    }

  }
  function car()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();

      if ($this->session->userdata('level')=='setda') {
        $data['car_setda_view'] = $this->Back_model->getCarForSetda();
        $this->load->view('view_car2',$data);
      } else {
        $data['car'] = $this->Back_model->getCAR();
        $this->load->view('view_car',$data);
      }
    } else {
      redirect('login');
    }

  }
  function import_kap()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_kap',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  function kap_ac()
  {
    if ($this->session->userdata('status')=='login') {
            // config upload
            $config['upload_path'] = './tmp_excel_upload/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);
			
			$cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'t_kap');
			
			if($cek_periode->num_rows() > 0)
			{
				$this->session->set_flashdata('periode_exist','Periode Sudah Ada');
				redirect(base_url('app/import_kap'));
			}
			else
			{
				if ( ! $this->upload->do_upload('kap')) {
                // jika validasi file gagal, kirim parameter error ke index
                $error = array('error' => $this->upload->display_errors());
                $this->index($error);
				} else {
					// jika berhasil upload ambil data dan masukkan ke database
					$upload_data = $this->upload->data();
					$file_name = $upload_data['file_name'];
					$extension = $upload_data['file_ext'];

					if ($extension == '.xlsx') {
					  $objReader = PHPExcel_IOFactory::createReader('Excel2007');
					} else {
					  $objReader = PHPExcel_IOFactory::createReader('Excel5');
					}

					$objReader->setReadDataOnly(true);

					//jika kosongkan data dicentang jalankan kode berikut
								$drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
								if($drop == 1){
							  //kosongkan tabel neraca
										 $this->Back_model->truncate_kap();
								};

					$objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
					$totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
					$objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

					for ($i=8; $i <= $totalrows; $i++) {
					  $data = array(
						'id'=>'',
						'keterangan'=>$objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() : ' ',
						'kredit_bln_lalu'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
						'bi_aba_non_giro_bln_lalu'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
						'jml_bln_lalu'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
						'kredit_saat_ini'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
						'bi_aba_non_giro_saat_ini'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
						'jml_saat_ini'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() : ' ',
						'periode'=>$this->input->post('periode'),
						'tgl'=>date('Ymd'),
						'status'=>'0'
					  );

					  $this->Back_model->insertKAP($data);
					}

					echo $totalrows;
					// delete image (for demo purpose only)
					$this->load->helper('file');
					delete_files('./tmp_excel_upload/');

					$this->session->set_flashdata("kap_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
					redirect(base_url('app/import_kap'));
				}
			}
			
            
    } else {
      redirect(base_url('login'));
    }

  }

  function kap()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      if ($this->session->userdata('level')=='setda') {
        $data['kap_setda_view'] = $this->Back_model->getKapForSetda();
        $this->load->view('view_kap2',$data);
      } else {
        $data['kap'] = $this->Back_model->getKAP();
        $this->load->view('view_kap',$data);
      }

    } else {
      redirect(base_url('login'));
    }

  }

  function import_ppap()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_ppap',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function ppap_ac()
  {
    if ($this->session->userdata('status')=='login') {
      // config upload
      $config['upload_path'] = './tmp_excel_upload/';
      $config['allowed_types'] = 'xls|xlsx';
      $config['max_size'] = '10000';
      $this->load->library('upload', $config);
		
		$cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'t_ppap');
		
		if($cek_periode->num_rows() > 0)
		{
			$this->session->set_flashdata('periode_exist','Periode Sudah Ada');
			redirect(base_url('app/import_ppap'));
		}
		else
		{
		  if ( ! $this->upload->do_upload('ppap')) {
          // jika validasi file gagal, kirim parameter error ke index
          $error = array('error' => $this->upload->display_errors());
          $this->index($error);
		  } else {
			  // jika berhasil upload ambil data dan masukkan ke database
			  $upload_data = $this->upload->data();
			  $file_name = $upload_data['file_name'];
			  $extension = $upload_data['file_ext'];

			  if ($extension == '.xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			  } else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
			  }

			  $objReader->setReadDataOnly(true);

			  //jika kosongkan data dicentang jalankan kode berikut
			  $drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
			  if($drop == 1){
			  //kosongkan tabel neraca
				   $this->Back_model->truncate_ppap();
			  };

			  $objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
			  $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
			  $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

			  for ($i=5; $i <= $totalrows; $i++) {
				$data = array(
				  'id'=>'',
				  'keterangan'=>$objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() : ' ',
				  'lancar'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
				  'kurang_lancar'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
				  'diragukan'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
				  'macet'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
				  'jumlah'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
				  'periode'=>$this->input->post('periode'),
				  'tgl'=>date('Ymd'),
				  'status'=>'0'
				);

				$this->Back_model->insertPPAP($data);
			  }

			  echo $totalrows;
			  // delete image (for demo purpose only)
			  $this->load->helper('file');
			  delete_files('./tmp_excel_upload/');

			  $this->session->set_flashdata("ppap_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
			  redirect(base_url('app/import_ppap'));
		  }
		}
      
    } else {
      redirect(base_url('login'));
    }

  }

  public function ppap()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      if ($this->session->userdata('level')=='setda') {
        $data['ppap_setda_view'] = $this->Back_model->getPpapForSetda();
        $this->load->view('view_ppap2',$data);
      } else {
        $data['ppap'] = $this->Back_model->getPPAP();
        $this->load->view('view_ppap',$data);
      }

    } else {
      redirect(base_url('login'));
    }

  }

  public function import_manajemen()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_manajemen',$data);
    } else {
      redirect(base_url('login'));
    }

  }
  public function manajemen_ac()
  {
    if ($this->session->userdata('status')=='login') {
      // config upload
      $config['upload_path'] = './tmp_excel_upload/';
      $config['allowed_types'] = 'xls|xlsx';
      $config['max_size'] = '10000';
      $this->load->library('upload', $config);
	  
	  $cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'t_manajemen');
	  
	  if($cek_periode->num_rows() > 0)
	  {
		  $this->session->set_flashdata('periode_exist','Periode Sudah Ada');
		  redirect(base_url('app/import_manajemen'));
	  }
	  else
	  {
		  if ( ! $this->upload->do_upload('manajemen')) {
          // jika validasi file gagal, kirim parameter error ke index
          $error = array('error' => $this->upload->display_errors());
          $this->index($error);
		  } else {
			  // jika berhasil upload ambil data dan masukkan ke database
			  $upload_data = $this->upload->data();
			  $file_name = $upload_data['file_name'];
			  $extension = $upload_data['file_ext'];

			  if ($extension == '.xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			  } else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
			  }

			  $objReader->setReadDataOnly(true);

			  //jika kosongkan data dicentang jalankan kode berikut
			  $drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
			  if($drop == 1){
			  //kosongkan tabel neraca
				   $this->Back_model->truncate_manajemen();
			  };

			  $objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
			  $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
			  $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

			  for ($i=6; $i <= $totalrows; $i++) {
				$data = array(
				  'id'=>'',
				  'penjelasan'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
				  'nol'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
				  'satu'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
				  'dua'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
				  'tiga'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
				  'empat'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() : ' ',
				  'periode'=>$this->input->post('periode'),
				  'tgl'=>date('Ymd'),
				  'status'=>'0'
				);

				$this->Back_model->insertManajemen($data);
			  }

			  echo $totalrows;
			  // delete image (for demo purpose only)
			  $this->load->helper('file');
			  delete_files('./tmp_excel_upload/');

			  $this->session->set_flashdata("manajemen_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
			  redirect(base_url('app/import_manajemen'));
		  }
	  }

      
    } else {
      redirect(base_url('login'));
    }

  }
  public function manajemen()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      if ($this->session->userdata('level')=='setda') {
        $data['manajemen_setda_view'] = $this->Back_model->getManajemenForSetda();
        $this->load->view('view_manajemen2',$data);
      } else {
        $data['manajemen'] = $this->Back_model->getManajemen();
        $this->load->view('view_manajemen',$data);
      }

    } else {
      redirect(base_url('login'));
    }

  }

  public function import_roa()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_roa',$data);
    } else {
      redirect(base_url('login'));
    }

  }
  public function roa_ac()
  {
    if ($this->session->userdata('status')=='login') {
      // config upload
      $config['upload_path'] = './tmp_excel_upload/';
      $config['allowed_types'] = 'xls|xlsx';
      $config['max_size'] = '10000';
      $this->load->library('upload', $config);
	  
	  $cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'t_roa');
	  if($cek_periode->num_rows() > 0)
	  {
		  $this->session->set_flashdata('periode_exist','Periode Sudah Ada');
		  redirect(base_url('app/import_roa'));
	  }
	  else
	  {
		  if ( ! $this->upload->do_upload('roa')) {
          // jika validasi file gagal, kirim parameter error ke index
          $error = array('error' => $this->upload->display_errors());
          $this->index($error);
		  } else {
			  // jika berhasil upload ambil data dan masukkan ke database
			  $upload_data = $this->upload->data();
			  $file_name = $upload_data['file_name'];
			  $extension = $upload_data['file_ext'];

			  if ($extension == '.xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			  } else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
			  }

			  $objReader->setReadDataOnly(true);

			  //jika kosongkan data dicentang jalankan kode berikut
			  $drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
			  if($drop == 1){
			  //kosongkan tabel neraca
				   $this->Back_model->truncate_roa();
			  };

			  $objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
			  $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
			  $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

			  for ($i=7; $i <= $totalrows; $i++) {
				$data = array(
				  'id'=>'',
				  'bulan'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
				  'vol_usaha'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
				  'laba_th_jalan'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
				  'net_laba'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
				  'periode'=>$this->input->post('periode'),
				  'tgl'=>date('Ymd'),
				  'status'=>'0'
				);

				$this->Back_model->insertROA($data);
			  }

			  echo $totalrows;
			  // delete image (for demo purpose only)
			  $this->load->helper('file');
			  delete_files('./tmp_excel_upload/');

			  $this->session->set_flashdata("roa_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
			  redirect(base_url('app/import_roa'));
		  }
	  }
  
    } else {
      redirect(base_url('login'));
    }

  }
  public function roa()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();

      if ($this->session->userdata('level')=='setda') {
        $data['roa_setda_view'] = $this->Back_model->getRoaForSetda();
        $this->load->view('view_roa2',$data);
      } else {
        $data['roa'] = $this->Back_model->getROA();
        $this->load->view('view_roa',$data);
      }

    } else {
      redirect(base_url('login'));
    }

  }
  function import_bopo()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_bopo',$data);
    } else {
      redirect(base_url('login'));
    }
  }
  function bopo_ac()
  {
    if ($this->session->userdata('status')=='login') {
      // config upload
      $config['upload_path'] = './tmp_excel_upload/';
      $config['allowed_types'] = 'xls|xlsx';
      $config['max_size'] = '10000';
      $this->load->library('upload', $config);
	  
	  $cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'t_bopo');
	  if($cek_periode->num_rows() > 0)
	  {
		  $this->session->set_flashdata('periode_exist','Periode Sudah Ada');
		  redirect(base_url('app/import_bopo'));
	  }
      else
	  {
		  if ( ! $this->upload->do_upload('bopo')) {
          // jika validasi file gagal, kirim parameter error ke index
          $error = array('error' => $this->upload->display_errors());
          $this->index($error);
		  } else {
			  // jika berhasil upload ambil data dan masukkan ke database
			  $upload_data = $this->upload->data();
			  $file_name = $upload_data['file_name'];
			  $extension = $upload_data['file_ext'];

			  if ($extension == '.xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			  } else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
			  }

			  $objReader->setReadDataOnly(true);

			  //jika kosongkan data dicentang jalankan kode berikut
			  $drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
			  if($drop == 1){
			  //kosongkan tabel neraca
				   $this->Back_model->truncate_bopo();
			  };

			  $objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
			  $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
			  $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

			  for ($i=7; $i <= $totalrows; $i++) {
				$data = array(
				  'id'=>'',
				  'bulan'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
				  'pend_operasional'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
				  'net_pend_operasional'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
				  'bi_operasional'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
				  'net_bi_operasional'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
				  'periode'=>$this->input->post('periode'),
				  'tgl'=>date('Ymd'),
				  'status'=>'0'
				);

				$this->Back_model->insertBOPO($data);
			  }

			  echo $totalrows;
			  // delete image (for demo purpose only)
			  $this->load->helper('file');
			  delete_files('./tmp_excel_upload/');

			  $this->session->set_flashdata("bopo_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
			  redirect(base_url('app/import_bopo'));
		  }
	  }

      
    } else {
      redirect(base_url('login'));
    }
  }
  public function bopo()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      if ($this->session->userdata('level')=='setda') {
        $data['bopo_setda_view'] = $this->Back_model->getBopoForSetda();
        $this->load->view('view_bopo2',$data);
      } else {
        $data['bopo'] = $this->Back_model->getBOPO();
        $this->load->view('view_bopo',$data);
      }

    } else {
      redirect(base_url('login'));
    }
  }

  public function import_cr_ldr()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('import_cr_ldr',$data);
    } else {
      redirect(base_url('login'));
    }

  }
  public function cr_ldr_ac()
  {
    if ($this->session->userdata('status')=='login') {
      // config upload
      $config['upload_path'] = './tmp_excel_upload/';
      $config['allowed_types'] = 'xls|xlsx';
      $config['max_size'] = '10000';
      $this->load->library('upload', $config);
	  
	  $cek_periode = $this->Back_model->cek_periode($this->input->post('periode'),'t_cr_ldr');
	  if($cek_periode->num_rows() > 0)
	  {
		  $this->session->set_flashdata('cek_periode','Periode Sudah Ada');
		  redirect(base_url('app/import_cr_ldr'));
	  }	
	  else
	  {
		  if ( ! $this->upload->do_upload('cr_ldr')) {
          // jika validasi file gagal, kirim parameter error ke index
          $error = array('error' => $this->upload->display_errors());
          $this->index($error);
		  } else {
			  // jika berhasil upload ambil data dan masukkan ke database
			  $upload_data = $this->upload->data();
			  $file_name = $upload_data['file_name'];
			  $extension = $upload_data['file_ext'];

			  if ($extension == '.xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			  } else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
			  }

			  $objReader->setReadDataOnly(true);

			  //jika kosongkan data dicentang jalankan kode berikut
			  $drop = isset( $_POST["drop"] ) ? $_POST["drop"] : 0 ;
			  if($drop == 1){
			  //kosongkan tabel neraca
				   $this->Back_model->truncate_cr_ldr();
			  };

			  $objPHPExcel = $objReader->load(FCPATH.'tmp_excel_upload/'.$file_name);
			  $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
			  $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

			  for ($i=6; $i <= $totalrows; $i++) {
				$data = array(
				  'id'=>'',
				  'pos_neraca'=>$objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(0,$i)->getValue() : ' ',
				  'p_bln_lalu'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
				  'p_saat_ini'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
				  'periode'=>$this->input->post('periode'),
				  'tgl'=>date('Ymd'),
				  'status'=>'0'
				);

				$this->Back_model->insert_cr_ldr($data);
			  }

			  echo $totalrows;
			  // delete image (for demo purpose only)
			  $this->load->helper('file');
			  delete_files('./tmp_excel_upload/');

			  $this->session->set_flashdata("cr_ldr_import_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Berhasil Import</div>");
			  redirect(base_url('app/import_cr_ldr'));
		  }
	  }

      
    } else {
      redirect(base_url('login'));
    }
  }
  public function cr_ldr()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();

      if ($this->session->userdata('level')=='setda') {
        $data['cr_ldr_setda_view'] = $this->Back_model->get_cr_ldr_forSetda();
        $this->load->view('view_cr_ldr2',$data);
      } else {
        $data['cr_ldr'] = $this->Back_model->get_cr_ldr();
        $this->load->view('view_cr_ldr',$data);
      }

    } else {
      redirect(base_url('login'));
    }

  }

  public function manage_user()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['account'] = $this->Back_model->getAllAccount();
      $this->load->view('manage_user',$data);
    } else {
      redirect('login');
    }
  }

  public function edit_account($id_account)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['account'] = $this->Back_model->getAccountById($id_account);
      $this->load->view('edit_user',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_account2($id_account)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['account'] = $this->Back_model->getAccountById($id_account);
      $this->load->view('edit_user2',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_account_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id_account = $this->input->post('id_user');
      $data = array(
        'username'=>$this->input->post('username'),
        'nama'=>$this->input->post('nama'),
        'level'=>$this->input->post('level'),
        'password'=>md5($this->input->post('password'))
      );
      $this->Back_model->update_account($id_account,$data);
      $this->session->set_flashdata("update_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated!</div>");
      $this->session->set_userdata('nama',$this->input->post('nama'));
      redirect(base_url("app/edit_account/".$id_account));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_account_ac2()
  {
    if ($this->session->userdata('status')=='login') {
      $id_account = $this->input->post('id_user');
      $data = array(
        'nama'=>$this->input->post('nama'),
        'password'=>md5($this->input->post('password'))
      );
      $this->Back_model->update_account($id_account,$data);
      $this->session->set_flashdata("update_success","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated!</div>");
      $this->session->set_userdata('nama',$this->input->post('nama'));
      redirect(base_url("app/edit_account2/".$id_account));
    } else {
      redirect(base_url('login'));
    }

  }

  public function setting_website()
  {
    if ($this->session->userdata('status')=='login') {
      if (isset($_POST['btnSubmit'])) {
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'jpg|jpeg|png|bpm';
        $config['max_size'] = '8192';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('logo')) {
          // jika validasi file gagal, kirim parameter error ke index
          $error = array('error' => $this->upload->display_errors());
          $this->index($error);
          $data = array(
            'nama'=>$this->input->post('judul_web'),
            'alamat'=>$this->input->post('alamat'),
            'kepsek'=>$this->input->post('kepsek'),
            'nip_kepsek'=>$this->input->post('nip'),
            'no_telp'=>$this->input->post('tlp'),
            'email'=>$this->input->post('email'),
            'tentang_singkat'=>$this->input->post('tentang_singkat')
          );
          $this->Back_model->setting_web($data);
          $this->session->set_flashdata("setting_berhasil","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated! </div>");
          redirect('app/setting_website');
        } else {
          $upload_data = $this->upload->data();
          $data = array(
            'nama'=>$this->input->post('judul_web'),
            'alamat'=>$this->input->post('alamat'),
            'kepsek'=>$this->input->post('kepsek'),
            'nip_kepsek'=>$this->input->post('nip'),
            'logo'=>$upload_data['file_name'],
            'no_telp'=>$this->input->post('tlp'),
            'email'=>$this->input->post('email'),
            'tentang_singkat'=>$this->input->post('tentang_singkat')
          );
          $this->Back_model->setting_web($data);
          $this->session->set_flashdata("setting_berhasil","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated! 1</div>");
          redirect('app/setting_website');
        }

      } else {
        $data['notif_petugas'] = $this->Back_model->getNotif();
        $data['instansi'] = $this->Back_model->getInstansi();
        $data['data_instansi'] = $this->Back_model->instansi();
        $this->load->view('setting_web',$data);
      }
    } else {
      redirect(base_url('login'));
    }

  }

  public function konfirmasi()
  {
    if ($this->session->userdata('status')=='login') {
      $data['not_confirm'] = $this->Back_model->getDataNotConfirm();
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('konfirmasi',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function konfirmasi_ac()
  {
    if ($this->session->userdata('status')=='login') {
      if (isset($_POST['konfirmasi'])) {
        $table = $this->input->post('table');
        $periode = $this->input->post('periode');
        $data = array(
          'status'=>'1'
        );
        $this->Back_model->konfirmasi($periode,$data,$table);
        $this->session->set_flashdata('konfirmasi_sukses',"<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data sudah dikonfirmasi!</div>");
        redirect(base_url('app/konfirmasi'));
		  } else {
			$periode = $this->input->post('periode');
			$data = array(
			  'id_notif'=>'',
			  'dari'=>'Pimpinan',
			  'pesan'=>$this->input->post('ket'),
			  'status'=>'unread',
			  'tgl'=>date('Ymd')
			);
			$this->Back_model->send_notif($data);
			$this->session->set_flashdata('notifikasi_sukses',"<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Notifikasi sudah dikirim!</div>");
			redirect(base_url('app/konfirmasi'));
		  }

    } else {
      redirect(base_url('login'));
    }

  }

  function acc_notif()
  {
    if ($this->session->userdata('status')=='login') {
      # code...
    } else {
      redirect(base_url('login'));
    }

  }

  function show_message($id_msg)
  {
    if ($this->session->userdata('status')=='login') {
      $this->Back_model->update_status_msg($id_msg);
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['msg_content'] = $this->Back_model->getInboxById($id_msg);
      $this->load->view('show_message',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function undo_import()
  {
    if ($this->session->userdata('status')=='login') {
      if (isset($_POST['btnSubmit'])) {
        $table = $this->input->post('table');
        $periode = $this->input->post('periode');

        $this->Back_model->undo_import($table,$periode);
        $this->session->set_flashdata('pembatalan_sukses',"<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Import data berhasil dibatalkan!</div>");
        redirect(base_url('app/undo_import'));
      } else {
        $data['notif_petugas'] = $this->Back_model->getNotif();
        $data['instansi'] = $this->Back_model->getInstansi();
        $this->load->view('undo_import',$data);
      }

    } else {
      redirect(base_url('login'));
    }

  }

  function edit_rekap($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['rekap'] = $this->Back_model->getRekapById($id);
      $this->load->view('edit_rekap',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_rekap_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'rasio'=>$this->input->post('rasio'),
        'nkk'=>$this->input->post('nkk'),
        'bobot'=>$this->input->post('bobot'),
        'nkf'=>$this->input->post('nkf'),
        'predikat'=>$this->input->post('predikat'),
        'periode'=>$this->input->post('periode')
      );
      $table = 't_rekap';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/rekap'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_neraca($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['neraca'] = $this->Back_model->getNeracaById($id);
      $this->load->view('edit_neraca',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_neraca_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'nopos'=>$this->input->post('nopos'),
        'nmpos'=>$this->input->post('nmpos'),
        'saldoBK'=>$this->input->post('saldoBK'),
        'debet'=>$this->input->post('debet'),
        'kredit'=>$this->input->post('kredit'),
        'saldo'=>$this->input->post('saldo'),
        'Periode'=>$this->input->post('periode')
      );
      $table = 'neraca';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/neraca'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_labarugi($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['labarugi'] = $this->Back_model->getLabarugiById($id);
      $this->load->view('edit_labarugi',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_labarugi_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'nopos'=>$this->input->post('nopos'),
        'nmpos'=>$this->input->post('nmpos'),
        'saldoBK'=>$this->input->post('saldoBK'),
        'debet'=>$this->input->post('debet'),
        'kredit'=>$this->input->post('kredit'),
        'saldo'=>$this->input->post('saldo'),
        'Periode'=>$this->input->post('periode'),
      );
      $table = 'labarugi';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/labarugi'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_car($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['car'] = $this->Back_model->getCarById($id);
      $this->load->view('edit_car',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_car_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'keterangan'=>$this->input->post('keterangan'),
        'p_bln_lalu'=>$this->input->post('p_bln_lalu'),
        'p_saat_ini'=>$this->input->post('p_saat_ini'),
        'periode'=>$this->input->post('periode')
      );
      $table = 't_car';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/car'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_kap($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['kap'] = $this->Back_model->getKAPById($id);
      $this->load->view('edit_kap',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_kap_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'keterangan'=>$this->input->post('keterangan'),
        'kredit_bln_lalu'=>$this->input->post('kredit_bln_lalu'),
        'bi_aba_non_giro_bln_lalu'=>$this->input->post('bi_aba_non_giro_bln_lalu'),
        'jml_bln_lalu'=>$this->input->post('jml_bln_lalu'),
        'kredit_saat_ini'=>$this->input->post('kredit_saat_ini'),
        'bi_aba_non_giro_saat_ini'=>$this->input->post('bi_aba_non_giro_saat_ini'),
        'jml_saat_ini'=>$this->input->post('jml_saat_ini'),
        'periode'=>$this->input->post('periode')
      );
      $table = 't_kap';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/kap'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_ppap($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['ppap'] = $this->Back_model->getPPAPById($id);
      $this->load->view('edit_ppap',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_ppap_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'keterangan'=>$this->input->post('keterangan'),
        'lancar'=>$this->input->post('lancar'),
        'kurang_lancar'=>$this->input->post('kurang_lancar'),
        'diragukan'=>$this->input->post('diragukan'),
        'macet'=>$this->input->post('macet'),
        'jumlah'=>$this->input->post('jumlah'),
        'periode'=>$this->input->post('periode')
      );
      $table = 't_ppap';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/ppap'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_manajemen($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['manajemen'] = $this->Back_model->getManajemenById($id);
      $this->load->view('edit_manajemen',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_manajemen_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'penjelasan'=>$this->input->post('penjelasan'),
        'nol'=>$this->input->post('nol'),
        'satu'=>$this->input->post('satu'),
        'dua'=>$this->input->post('dua'),
        'tiga'=>$this->input->post('tiga'),
        'empat'=>$this->input->post('empat'),
        'periode'=>$this->input->post('periode')
      );
      $table = 't_manajemen';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/manajemen'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_roa($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['roa'] = $this->Back_model->getRoaById($id);
      $this->load->view('edit_roa',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_roa_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'bulan'=>$this->input->post('bulan'),
        'vol_usaha'=>$this->input->post('vol_usaha'),
        'laba_th_jalan'=>$this->input->post('laba_th_jalan'),
        'net_laba'=>$this->input->post('net_laba'),
        'periode'=>$this->input->post('periode')
      );
      $table = 't_roa';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/roa'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_bopo($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['bopo'] = $this->Back_model->getBopoById($id);
      $this->load->view('edit_bopo',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_bopo_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'bulan'=>$this->input->post('bulan'),
        'pend_operasional'=>$this->input->post('pend_operasional'),
        'net_pend_operasional'=>$this->input->post('net_pend_operasional'),
        'bi_operasional'=>$this->input->post('bi_operasional'),
        'net_bi_operasional'=>$this->input->post('net_bi_operasional'),
        'periode'=>$this->input->post('periode')
      );
      $table = 't_bopo';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/bopo'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_cr_ldr($id)
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['cr_ldr'] = $this->Back_model->getCrldrById($id);
      $this->load->view('edit_cr_ldr',$data);
    } else {
      redirect(base_url('login'));
    }

  }

  public function edit_crldr_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $id = $this->input->post('id');
      $data = array(
        'pos_neraca'=>$this->input->post('pos_neraca'),
        'p_bln_lalu'=>$this->input->post('p_bln_lalu'),
        'p_saat_ini'=>$this->input->post('p_saat_ini'),
        'periode'=>$this->input->post('periode')
      );
      $table = 't_cr_ldr';
      $this->Back_model->update_data($data,$id,$table);
      $this->session->set_flashdata("edit_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Data berhasil disimpan!</div>");
      redirect(base_url('app/cr_ldr'));
    } else {
      redirect(base_url('login'));
    }

  }

  public function tambah_user()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $this->load->view('tambah_user',$data);
    } else {
      redirect(base_url('login'));
    }

  }
  public function tambah_user_ac()
  {
    if ($this->session->userdata('status')=='login') {
      $data = array(
        'id_user'=>'',
        'username'=>$this->input->post('username'),
        'password'=>md5($this->input->post('password')),
        'nama'=>$this->input->post('nama'),
        'level'=>$this->input->post('level')
      );
      $this->Back_model->tambah_user($data);
      $this->session->set_flashdata("tambah_usr_sukses","<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>User telah ditambah</div>");
      redirect(base_url('app/manage_user'));
    } else {
      redirect(base_url('login'));
    }

  }
  public function delete_account($id_user)
  {
    if ($this->session->userdata('status')=='login') {
      $this->Back_model->delete_user($id_user);
      $this->session->set_flashdata("hapus_usr_sukses","<div class='alert alert-warning alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>User berhasil dihapus</div>");
      redirect(base_url('app/manage_user'));
    } else {
      redirect(base_url('login'));
    }

  }
  public function inbox()
  {
    if ($this->session->userdata('status')=='login') {
      $data['notif_petugas'] = $this->Back_model->getNotif();
      $data['instansi'] = $this->Back_model->getInstansi();
      $data['inbox'] = $this->Back_model->getInbox();
      $this->load->view('inbox',$data);
    } else {
      redirect(base_url('login'));
    }
    
  }
}
