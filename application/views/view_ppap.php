<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data PPAP</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tabelPPAP" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id</th>
              <th>KETERANGAN</th>
              <th>Lancar</th>
              <th>Kurang Lancar</th>
              <th>Diragukan</th>
              <th>Macet</th>
              <th>Jumlah</th>
              <th>Periode</th>
              <?php
              if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                ?>
                <th>Status</th>
                <?php
              }
              ?>
              <?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <th id="opsi">Opsi</th>
				  <?php
			  }
			  ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($ppap->result_array() as $ppap): ?>
              <tr>
                <td><?php echo $ppap["id"]; ?></td>
                <td><?php echo $ppap["keterangan"]; ?></td>
                <td><?php echo $ppap["lancar"]; ?></td>
                <td><?php echo $ppap["kurang_lancar"]; ?></td>
                <td><?php echo $ppap["diragukan"]; ?></td>
                <td><?php echo $ppap["macet"]; ?></td>
                <td><?php echo $ppap["jumlah"]; ?></td>
                <td><?php echo $ppap["periode"]; ?></td>
                <?php
                if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                  ?>
                  <td><?php
                    if ($ppap['status']=='0') {
                      echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                    } else {
                      echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                    }

                  ?></td>
                  <?php
                }
                ?>
				<?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <td id="opsi">
                  <?php if ($this->session->userdata('level')=='petugas'||($this->session->userdata('level')=='super_admin')): ?>
                    <a href="<?php echo base_url('app/edit_ppap'); ?>/<?php echo $ppap['id']; ?>" class="btn btn-warning">Edit</a>
                  <?php endif; ?>
                </td>
				  <?php
			  }
			  ?>
                
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>Id</th>
                <th>KETERANGAN</th>
                <th>Lancar</th>
                <th>Kurang Lancar</th>
                <th>Diragukan</th>
                <th>Macet</th>
                <th>Jumlah</th>
                <th>Periode</th>
                <?php
                if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                  ?>
                  <th>Status</th>
                  <?php
                }
                ?>
                <?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <th id="opsi">Opsi</th>
				  <?php
			  }
			  ?>
              </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#tabelPPAP").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
