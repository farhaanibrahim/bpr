<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<style media="screen">
  #myModal1 .modal-dialog  {
    width:75%;
  }
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pembatalan Import Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Form pembatalan</h3>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="box-body">
            <?php echo $this->session->flashdata('pembatalan_sukses'); ?>

            <div class="col-md-4">
              <form class="" action="<?php echo base_url('app/undo_import'); ?>" method="post">
                <div class="form-group">
                  <label for="">Pilih tabel yang akan di batalkan</label>
                  <select class="form-control" name="table" required>
                    <option value="">-- Pilih Tabel disini --</option>
                    <option value="t_rekap">Rekap</option>
                    <option value="neraca">Neraca</option>
                    <option value="labarugi">Labarugi</option>
                    <option value="t_car">Capital Adequacy Ratio (CAR)</option>
                    <option value="t_kap">Kualitas Aktiva Produktif (KAP)</option>
                    <option value="t_ppap">PPAP</option>
                    <option value="t_manajemen">Manajemen</option>
                    <option value="t_roa">Return On Asset (ROA)</option>
                    <option value="t_bopo">BOPO</option>
                    <option value="t_cr_ldr">CR & LDR</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Periode</label>
                  <input type="text" name="periode" id="periode" placeholder="Periode" class="form-control" required>
                </div>
                <div class="form-group">
                  <input type="submit" name="btnSubmit" onclick="confirm('Apakah anda sudah yakin untuk melakukan pembatalan?');" value="Batalkan" class="btn btn-danger">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>


<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
