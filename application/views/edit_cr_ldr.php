<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

<?php foreach ($cr_ldr->result() as $row) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit Data CR & LDR</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/edit_crldr_ac'); ?>" method="post">
            <div class="col-md-4">
              <input type="hidden" name="id" value="<?php echo $row->id; ?>">
              <div class="form-group">
                <label>Pos Neraca</label>
                <input type="text" name="pos_neraca" value="<?php echo $row->pos_neraca; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Posisi Bulan Lalu</label>
                <input type="text" name="p_bln_lalu" value="<?php echo $row->p_bln_lalu; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Posisi Saat Ini</label>
                <input type="text" name="p_saat_ini" value="<?php echo $row->p_saat_ini; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Periode</label>
                <input type="text" name="periode" id="periode" value="<?php echo $row->periode; ?>" class="form-control">
              </div>
              <input type="submit" name="btnSubmit" class="btn btn-primary" value="Simpan">
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
