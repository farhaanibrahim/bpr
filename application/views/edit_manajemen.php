<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

<?php foreach ($manajemen->result() as $row) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit Data Manajemen</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/edit_manajemen_ac'); ?>" method="post">
            <div class="col-md-4">
              <input type="hidden" name="id" value="<?php echo $row->id; ?>">
              <div class="form-group">
                <label>Penjelasan</label>
                <textarea name="penjelasan" class="form-control"><?php echo $row->penjelasan; ?></textarea>
              </div>
              <div class="form-group">
                <label>0 (Nol)</label>
                <input type="text" name="nol" value="<?php echo $row->nol; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>1 (Satu)</label>
                <input type="text" name="satu" value="<?php echo $row->satu; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>2 (Dua)</label>
                <input type="text" name="dua" value="<?php echo $row->dua; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>3 (Tiga)</label>
                <input type="text" name="tiga" value="<?php echo $row->tiga; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>4 (Empat)</label>
                <input type="text" name="empat" value="<?php echo $row->empat; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Periode</label>
                <input type="text" name="periode" id="periode" value="<?php echo $row->periode; ?>" class="form-control">
              </div>
              <input type="submit" name="btnSubmit" value="Simpan" class="btn btn-primary">
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
