<?php
  include_once'template/header.php';
  include_once'template/side.php';

  foreach ($account->result_array() as $account) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      User
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit User</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-md-6">
            <form action="<?php echo base_url("app/edit_account_ac2/"); ?>" method="post">
              <?php echo $this->session->flashdata('update_success'); ?>
              <input type="hidden" name="id_user" value="<?php echo $account['id_user']; ?>">
              <div class="form-group">
                <label for="">Username</label>
                <input type="text" name="username" value="<?php echo $account['username']; ?>" class="form-control" disabled>
              </div>
              <div class="form-group">
                <label for="">Nama</label>
                <input type="text" name="nama" value="<?php echo $account['nama']; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password" required>
              </div>
              <input type="submit" name="btnSubmit" value="Update" class="btn btn-primary">
            </form>
          </div>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
