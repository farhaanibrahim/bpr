<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manajemen User
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Tambah User</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-md-4">
            <form action="<?php echo base_url('app/tambah_user_ac'); ?>" method="post">
              <div class="form-group">
                <label for="">Username</label>
                <input type="text" name="username" class="form-control" placeholder="Username" required="">
              </div>
              <div class="form-group">
                <label for="">Nama Lengkap</label>
                <input type="text" name="nama" class="form-control" placeholder="Name" required="">
              </div>
              <div class="form-group">
                <label for="">Level</label>
                <select name="level" class="form-control" required="">
                  <option value="">-- Pilih Level User --</option>
                  <option value="pimpinan">Pimpinan</option>
                  <option value="petugas">Petugas</option>
                  <option value="setda">SETDA</option>
                </select>
              </div>
              <div class="form-group">
                <label for="">Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
              </div>
              <input type="submit" name="btnSubmit" value="Tambah" class="btn btn-warning">
            </form>
          </div>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
