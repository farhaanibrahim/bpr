<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

<?php foreach ($bopo->result() as $row) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit Data BOPO</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/edit_bopo_ac'); ?>" method="post">
            <div class="col-md-4">
              <input type="hidden" name="id" value="<?php echo $row->id; ?>">
              <div class="form-group">
                <label>Bulan</label>
                <input type="text" name="bulan" value="<?php echo $row->bulan; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Pendapatan Operasional</label>
                <input type="text" name="pend_operasional" value="<?php echo $row->pend_operasional; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Net Pend. Operasional</label>
                <input type="text" name="net_pend_operasional" value="<?php echo $row->net_pend_operasional; ?>" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Biaya Operasional</label>
                <input type="text" name="bi_operasional" value="<?php echo $row->bi_operasional; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Net Bi. Operasional</label>
                <input type="text" name="net_bi_operasional" value="<?php echo $row->net_bi_operasional; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Periode</label>
                <input type="text" name="periode" id="periode" value="<?php echo $row->periode; ?>" class="form-control">
              </div>
              <div class="form-group" align="right">
                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Simpan">
              </div>
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
