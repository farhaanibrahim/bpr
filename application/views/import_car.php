<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Import Data Capital Adequacy Ratio
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Input Data</h3>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="box-body">
            <?php echo $this->session->flashdata('car_import_success'); ?>
			<?php
			if($this->session->flashdata('periode_exist'))
			{
			?>
				<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><?php echo $this->session->flashdata('periode_exist'); ?></h4></div>
			<?php
			}
			?>
            <div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4>Pastikan Extensi File Excel yang digunakan (.xls) untuk format excel anda bisa download  <a href="<?php echo base_url('assets/excel_examples'); ?>/car-des2016.xls">di sini</a></h4></div>

            <form name="myForm" id="myForm" onSubmit="return validateFormCar()" action="<?php echo base_url('app/car_ac'); ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <input type="file" id="car" name="car" required /><br />
              </div>
              <div class="form-group">
                <label>Periode : </label>
                <input type="text" id="periode" name="periode" class="form-control" required><br>
              </div>
              <div class="form-group">
                <input type="submit" name="btnSubmit" class="btn btn-success" value="Import" /><br/>
                <!--
                <label><input type="checkbox" name="drop" value="1" /> <u>Kosongkan tabel sql terlebih dahulu.</u> </label>
                -->
              </div>
            </form>
            <script type="text/javascript">
              //    validasi form (hanya file .xls yang diijinkan)
                function validateFormCar()
                {
                  function hasExtension(inputID, exts) {
                    var fileName = document.getElementById(inputID).value;
                    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
                  }

                  if(!hasExtension('car', ['.xls','.xlsx'])){
                    alert("Hanya file XLS (Excel 2003) yang diijinkan.");
                    return false;
                  }
                }
              </script>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
