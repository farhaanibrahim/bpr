<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data CR & LDR</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tabel_cr_ldr" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id</th>
              <th>Pos-pos Neraca</th>
              <th>Posisi Bulan Lalu</th>
              <th>Posisi Saat Ini</th>
              <th>Periode</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($cr_ldr_setda_view->result_array() as $cr_ldr): ?>
              <tr>
                <td><?php echo $cr_ldr["id"]; ?></td>
                <td><?php echo $cr_ldr["pos_neraca"]; ?></td>
                <td><?php echo $cr_ldr["p_bln_lalu"]; ?></td>
                <td><?php echo $cr_ldr["p_saat_ini"]; ?></td>
                <td><?php echo $cr_ldr["periode"]; ?></td>
                <td><?php
                  if ($cr_ldr['status']=='0') {
                    echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                  } else {
                    echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                  }

                ?></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>Id</th>
                <th>Pos-pos Neraca</th>
                <th>Posisi Bulan Lalu</th>
                <th>Posisi Saat Ini</th>
                <th>Periode</th>
                <th>Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#tabel_cr_ldr").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
