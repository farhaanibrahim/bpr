<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data Rekap</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tabelRekap" class="table table-bordered">
            <thead>
            <tr>
              <th>No.</th>
              <th>Faktor</th>
              <th>Rasio</th>
              <th>Nilai Kredit Komponen</th>
              <th>Bobot</th>
              <th>Nilai Kredit Faktor</th>
              <th>Predikat</th>
              <th>Periode</th>
              <?php
              if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                ?>
                <th>Status</th>
                <?php
              }
              ?>
			  <?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <th id="opsi">Opsi</th>
				  <?php
			  }
			  ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rekap->result_array() as $rekap): ?>
              <tr style="<?php if ($rekap['no'] != ' ') {
                  ?>
                  background: #bfecfc;
                  <?php
                } ?>
              ">
                <td><?php echo $rekap['no']; ?></td>
                <td> <a href="
                    <?php
                    if ($rekap['faktor'] == 'Capital') {
                      echo base_url('app/car');
                    } else if ($rekap['faktor'] == 'KAP') {
                      echo base_url('app/kap');
                    } else if ($rekap['faktor'] == 'Rasio PPAP') {
                      echo base_url('app/ppap');
                    } else if ($rekap['faktor'] == 'Management') {
                      echo base_url('app/manajemen');
                    } else if ($rekap['faktor'] == 'ROA') {
                      echo base_url('app/roa');
                    } else if ($rekap['faktor'] == 'BOPO') {
                      echo base_url('app/bopo');
                    } else if ($rekap['faktor'] == 'Cash Ratio') {
                      echo base_url('app/cr_ldr');
                    } else if ($rekap['faktor'] == 'LDR') {
                      echo base_url('app/cr_ldr');
                    }  else {
                      echo '#';
                    }
                    ?>
                  "><?php echo $rekap["faktor"]; ?></a></td>
                <td><?php echo $rekap["rasio"]; ?></td>
                <td><?php echo $rekap["nkk"]; ?></td>
                <td><?php echo $rekap["bobot"]; ?></td>
                <td><?php echo $rekap["nkf"]; ?></td>
                <td><?php echo $rekap["predikat"]; ?></td>
                <td><?php echo $rekap["periode"]; ?></td>
                <?php
                if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                  ?>
                  <td><?php
                    if ($rekap['status']=='0') {
                      echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                    } else {
                      echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                    }

                  ?></td>
                  <?php
                }
                ?>
				<?php
				if($this->session->userdata('level')!=='super_admin')
				{
					?>
					<td>
					  <?php if ($this->session->userdata('level')=='petugas'||$this->session->userdata('level')=='super_admin'): ?>
						<a href="<?php echo base_url('app/edit_rekap'); ?>/<?php echo $rekap['id']; ?>" class="btn btn-warning">Edit</a>
					  <?php endif; ?>
					</td>
					<?php
				}
				?>
				
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
              <th>Id</th>
              <th>Faktor</th>
              <th>Rasio</th>
              <th>Nilai Kredit Komponen</th>
              <th>Bobot</th>
              <th>Nilai Kredit Faktor</th>
              <th>Predikat</th>
              <th>Periode</th>
              <?php
              if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                ?>
                <th>Status</th>
                <?php
              }
              ?>
              <?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <th id="opsi">Opsi</th>
				  <?php
			  }
			  ?>
            </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $('#tabelRekap').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
