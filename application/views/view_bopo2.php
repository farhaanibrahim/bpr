<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data BOPO</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tabelBOPO" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id</th>
              <th>Bulan</th>
              <th>Pendapatan Operasional</th>
              <th>Net Pend. Operasional</th>
              <th>Biaya Operasional</th>
              <th>Net Bi. Operasional</th>
              <th>Periode</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($bopo_setda_view->result_array() as $bopo): ?>
              <tr>
                <td><?php echo $bopo["id"]; ?></td>
                <td><?php echo $bopo["bulan"]; ?></td>
                <td><?php echo $bopo["pend_operasional"]; ?></td>
                <td><?php echo $bopo["net_pend_operasional"]; ?></td>
                <td><?php echo $bopo["bi_operasional"]; ?></td>
                <td><?php echo $bopo["net_bi_operasional"]; ?></td>
                <td><?php echo $bopo["periode"]; ?></td>
                <td><?php
                  if ($bopo['status']=='0') {
                    echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                  } else {
                    echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                  }

                ?></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>Id</th>
                <th>Bulan</th>
                <th>Pendapatan Operasional</th>
                <th>Net Pend. Operasional</th>
                <th>Biaya Operasional</th>
                <th>Net Bi. Operasional</th>
                <th>Periode</th>
                <th>Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#tabelBOPO").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
