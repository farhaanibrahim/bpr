<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manajemen User
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">List User</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php
            echo $this->session->flashdata('tambah_usr_sukses');
            echo $this->session->flashdata('hapus_usr_sukses');
          ?>
          <a href="<?php echo base_url('app/tambah_user'); ?>" class="btn btn-primary">Tambah User (+)</a>
          <table class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id User</th>
              <th>Username</th>
              <th>Nama</th>
              <th>Level</th>
              <th>Opstion</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($account->result_array() as $account): ?>
              <tr>
                <td><?php echo $account["id_user"]; ?></td>
                <td><?php echo $account["username"]; ?></td>
                <td><?php echo $account["nama"]; ?></td>
                <td><?php echo $account["level"]; ?></td>
                <td>
                  <a href="<?php echo base_url('app/delete_account'); ?>/<?php echo $account['id_user']; ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus akun?')">Delete</a>
                  <a href="<?php echo base_url('app/edit_account'); ?>/<?php echo $account['id_user']; ?>" class="btn btn-warning">Edit</a>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
