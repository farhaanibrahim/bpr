<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

<?php foreach ($kap->result() as $row) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit Data Kualitas Aktiva Produktif (KAP)</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/edit_kap_ac'); ?>" method="post">
            <div class="col-md-6">
                <input type="hidden" name="id" value="<?php echo $row->id; ?>">
                <div class="form-group">
                  <label>Keterangan</label>
                  <input type="text" name="keterangan" value="<?php echo $row->keterangan; ?>" class="form-control">
                </div>

            </div>
            <div class="col-md-12">
              <div class="col-md-6">
                <u><h4>Posisi Bulan Lalu</h4></u>
                <div class="form-group">
                  <label>Kredit</label>
                  <input type="text" name="kredit_bln_lalu" value="<?php echo $row->kredit_bln_lalu; ?>" class="form-control">
                </div>
                <div class="form-group">
                  <label>BI+ABA non-Giro</label>
                  <input type="text" name="bi_aba_non_giro_bln_lalu" value="<?php echo $row->bi_aba_non_giro_bln_lalu; ?>" class="form-control">
                </div>
                <div class="form-group">
                  <label>Jumlah</label>
                  <input type="text" name="jml_bln_lalu" value="<?php echo $row->jml_bln_lalu; ?>" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <u><h4>Posisi Saat Ini</h4></u>
                <div class="form-group">
                  <label>Kredit</label>
                  <input type="text" name="kredit_saat_ini" value="<?php echo $row->kredit_saat_ini; ?>" class="form-control">
                </div>
                <div class="form-group">
                  <label>BI+ABA non-Giro</label>
                  <input type="text" name="bi_aba_non_giro_saat_ini" value="<?php echo $row->bi_aba_non_giro_saat_ini; ?>" class="form-control">
                </div>
                <div class="form-group">
                  <label>jml_saat_ini</label>
                  <input type="text" name="jml_saat_ini" value="<?php echo $row->jml_saat_ini; ?>" class="form-control">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Periode</label>
                <input type="text" name="periode" id="periode" value="<?php echo $row->periode; ?>" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div style="padding-top:25px;">

              </div>
              <input type="submit" name="btnSubmit" value="Simpan" class="btn btn-primary">
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
