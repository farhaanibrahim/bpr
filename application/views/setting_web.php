<?php
  include_once'template/header.php';
  include_once'template/side.php';

  foreach ($data_instansi->result_array() as $data_instansi) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Setting
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Setting Web</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/setting_website'); ?>" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
              <?php echo $this->session->flashdata('setting_berhasil'); ?>
              <div class="form-group">
                <label for="">Judul Website</label>
                <input type="text" name="judul_web" class="form-control" value="<?php echo $data_instansi['nama']; ?>">
              </div>
              <div class="form-group">
                <label for="">Kepala Bagian</label>
                <input type="text" name="kepsek" class="form-control" value="<?php echo $data_instansi['kepsek']; ?>">
              </div>
              <div class="form-group">
                <label for="">NIP Kepala Bagian</label>
                <input type="text" name="nip" class="form-control" value="<?php echo $data_instansi['nip_kepsek']; ?>">
              </div>
              <div class="form-group">
                <label for="">No. Telp</label>
                <input type="text" name="tlp" class="form-control" value="<?php echo $data_instansi['no_telp']; ?>">
              </div>
              <div class="form-group">
                <label for="">Email</label>
                <input type="text" name="email" class="form-control" value="<?php echo $data_instansi['email']; ?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Logo</label>
                <input type="file" name="logo" value="">
              </div>
              <div class="form-group">
                <label for="">Tentang Singkat</label>
                <textarea name="tentang_singkat" class="form-control"><?php echo $data_instansi['tentang_singkat']; ?></textarea>
              </div>
              <div class="form-group">
                <label for="">Alamat</label>
                <textarea name="alamat" class="form-control"><?php echo $data_instansi['alamat']; ?></textarea>
              </div>
            </div>
            <div class="col-md-12" align="right">
              <input type="submit" name="btnSubmit" value="Simpan" align="right" class="btn btn-warning">
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
