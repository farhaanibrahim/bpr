<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

<?php foreach ($rekap->result() as $row) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit Data Rekap</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/edit_rekap_ac'); ?>" method="post">
            <div class="col-md-4">
              <input type="hidden" name="id" value="<?php echo $row->id; ?>">
              <div class="form-group">
                <label>Faktor</label>
                <input type="text" name="faktor" value="<?php echo $row->faktor; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Rasio</label>
                <input type="text" name="rasio" value="<?php echo $row->rasio; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>NKK</label>
                <input type="text" name="nkk" value="<?php echo $row->nkk; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Bobot</label>
                <input type="text" name="bobot" value="<?php echo $row->bobot; ?>" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>NKF</label>
                <input type="text" name="nkf" value="<?php echo $row->nkf; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Predikat</label>
                <input type="text" name="predikat" value="<?php echo $row->predikat; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Periode</label>
                <input type="text" id="periode" name="periode" value="<?php echo $row->periode; ?>" class="form-control">
              </div>
              <div class="form-group" align="right">
                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Simpan">
              </div>
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
