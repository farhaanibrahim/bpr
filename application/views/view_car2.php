<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data Capital Adequacy Ratio</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tabelCar" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id</th>
              <th>KETERANGAN</th>
              <th>Posisi Bulan Lalu</th>
              <th>Posisi Saat Ini</th>
              <th>Periode</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($car_setda_view->result_array() as $car): ?>
              <tr>
                <td><?php echo $car["id"]; ?></td>
                <td><?php echo $car["keterangan"]; ?></td>
                <td><?php echo number_format($car["p_bln_lalu"],0); ?></td>
                <td><?php echo number_format($car["p_saat_ini"],0); ?></td>
                <td><?php echo $car["periode"]; ?></td>
                <td><?php
                  if ($car['status']=='0') {
                    echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                  } else {
                    echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                  }

                ?></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>Id</th>
                <th>KETERANGAN</th>
                <th>Posisi Bulan Lalu</th>
                <th>Posisi Saat Ini</th>
                <th>Periode</th>
                <th>Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $('#tabelCar').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>
