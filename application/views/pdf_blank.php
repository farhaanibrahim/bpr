<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css'); ?>/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins'); ?>/skin-yellow.min.css">

	<link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css"> 
</head>
<body>


<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/bootstrap/js'); ?>/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js'); ?>/app.min.js"></script>
</body>
</html>