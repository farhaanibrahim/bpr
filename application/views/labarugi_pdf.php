<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
</head>
<body>
<style type="text/css">
	.header {
		text-align: center;
	}
</style>
<!-- CSS goes in the document HEAD or added to your external stylesheet -->
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="header">
				<h2><?php echo "Data Labarugi ".$bulan." Tahun ".$tahun; ?></h2>
			</div>
			<br>
			<table class="gridtable">
				<thead>
					<tr>
						<th>Id</th>
						<th>No. Pos</th>
						<th>Nama Pos</th>
						<th>Saldu Bulan Kemarin</th>
						<th>Debet</th>
						<th>Kredit</th>
						<th>Saldo Bulan Ini</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($labarugi1->result() as $row): ?>
						<tr>
							<td><?php echo $row->id; ?></td>
							<td><?php echo $row->nopos; ?></td>
							<td><?php echo $row->nmpos; ?></td>
							<td><?php echo ($row->saldoBK != ' ' ? number_format((int)$row->saldoBK,0,",",".") : ' '); ?></td>
							<td><?php echo ($row->debet != ' ' ? number_format((int)$row->debet,0,",",".") : ' '); ?></td>
							<td><?php echo ($row->kredit != ' ' ? number_format((int)$row->kredit,0,",",".") : ' '); ?></td>
							<td><?php echo ($row->saldo != ' ' ? number_format((int)$row->saldo,0,",",".") : '  '); ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>