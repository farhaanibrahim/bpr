<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<?php 
  foreach ($car1->result() as $car1) {} 
  foreach ($car2->result() as $car2) {}
  foreach ($car3->result() as $car3) {}

  foreach ($kap1->result() as $kap1) {} 
  foreach ($kap2->result() as $kap2) {}
  foreach ($kap3->result() as $kap3) {}

  foreach ($ppap1->result() as $ppap1) {} 
  foreach ($ppap2->result() as $ppap2) {}
  foreach ($ppap3->result() as $ppap3) {}

  foreach ($manajemen1->result() as $manajemen1) {} 
  foreach ($manajemen2->result() as $manajemen2) {}
  foreach ($manajemen3->result() as $manajemen3) {}

  foreach ($roa1->result() as $roa1) {} 
  foreach ($roa2->result() as $roa2) {}
  foreach ($roa3->result() as $roa3) {}

  foreach ($bopo1->result() as $bopo1) {} 
  foreach ($bopo2->result() as $bopo2) {}
  foreach ($bopo3->result() as $bopo3) {}

  foreach ($cr_ldr1->result() as $cr_ldr1) {} 
  foreach ($cr_ldr2->result() as $cr_ldr2) {}
  foreach ($cr_ldr3->result() as $cr_ldr3) {}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $jml_rekap; ?></h3>

              <p>Data Rekap</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo base_url('app/rekap'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $jml_neraca; ?></h3>

              <p>Data Neraca</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo base_url('app/neraca'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $jml_labarugi; ?></h3>

              <p>Laba Rugi</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url('app/labarugi'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $jml_car; ?></h3>

              <p>Capital Asset Ratio</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?php echo base_url('app/car'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

      <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Data</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
                <br>
                <div class="col-md-12 text-center">
                  <!-- Legend -->
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once'template/footer.php'; ?>

<script type="text/javascript">
    var areaChartData = {
      labels: [<?php echo $tahun1; ?>, <?php echo $tahun2; ?>, <?php echo $tahun3; ?>],
      datasets: [
        {
          label: "CAR",
          fillColor: "#383636",
          strokeColor: "#383636",
          pointColor: "#383636",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [<?php echo (isset($car1->rasio) ? $car1->rasio : 0); ?>, <?php echo (isset($car2->rasio) ? $car2->rasio : 0); ?>, <?php echo (isset($car3->rasio) ? $car3->rasio : 0); ?>]
        },
        {
          label: "KAP",
          fillColor: "#bdb8ad",
          strokeColor: "#bdb8ad",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php echo (isset($kap1->rasio) ? $kap1->rasio : 0); ?>, <?php echo (isset($kap2->rasio) ? $kap2->rasio : 0); ?>, <?php echo (isset($kap3->rasio) ? $kap3->rasio : 0); ?>]
        },
        {
          label: "PPAP",
          fillColor: "#a7dbd8",
          strokeColor: "#a7dbd8",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php echo (isset($ppap1->rasio) ? $ppap1->rasio : 0); ?>, <?php echo (isset($ppap2->rasio) ? $ppap2->rasio : 0); ?>, <?php echo (isset($ppap3->rasio) ? $ppap3->rasio : 0); ?>]
        },
        {
          label: "Management",
          fillColor: "#e0e4cc",
          strokeColor: "#e0e4cc",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php echo (isset($manajemen1->nkk) ? $manajemen1->nkk : 0); ?>, <?php echo (isset($manajemen2->nkk) ? $manajemen2->nkk : 0); ?>, <?php echo (isset($manajemen3->nkk) ? $manajemen3->nkk : 0); ?>]
        },
        {
          label: "ROA",
          fillColor: "#f38630",
          strokeColor: "#f38630",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php echo (isset($roa1->rasio) ? $roa1->rasio : 0); ?>, <?php echo (isset($roa2->rasio) ? $roa2->rasio : 0); ?>, <?php echo (isset($roa3->rasio) ? $roa3->rasio : 0); ?>]
        },
        {
          label: "BOPO",
          fillColor: "#ff5a0b",
          strokeColor: "#ff5a0b",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php echo (isset($bopo1->rasio) ? $bopo1->rasio : 0); ?>, <?php echo (isset($bopo2->rasio) ? $bopo2->rasio : 0); ?>, <?php echo (isset($bopo3->rasio) ? $bopo3->rasio : 0); ?>]
        },
        {
          label: "CR LDR",
          fillColor: "#ff0000",
          strokeColor: "#ff0000",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php echo (isset($cr_ldr1->rasio) ? $cr_ldr1->rasio : 0); ?>, <?php echo (isset($cr_ldr2->rasio) ? $cr_ldr2->rasio : 0); ?>, <?php echo (isset($cr_ldr3->rasio) ? $cr_ldr3->rasio : 0); ?>]
        }
      ]
    };



    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 20,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 2,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true,
      legendDisplay: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
    </script>
