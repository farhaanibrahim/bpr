<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

<?php foreach ($roa->result() as $row) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit Data ROA</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/edit_roa_ac'); ?>" method="post">
            <div class="col-md-4">
              <input type="hidden" name="id" value="<?php echo $row->id; ?>">
              <div class="form-group">
                <label>Bulan</label>
                <input type="text" name="bulan" value="<?php echo $row->bulan; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Volume Usaha</label>
                <input type="text" name="vol_usaha" value="<?php echo $row->vol_usaha; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Laba Th Jalan</label>
                <input type="text" name="laba_th_jalan" value="<?php echo $row->laba_th_jalan; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Net Laba</label>
                <input type="text" name="net_laba" value="<?php echo $row->net_laba; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Periode</label>
                <input type="text" name="periode" id="periode" value="<?php echo $row->periode; ?>" class="form-control">
              </div>
              <input type="submit" name="btnSubmit" value="Simpan" class="btn btn-primary">
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
