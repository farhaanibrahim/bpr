<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data Manajemen</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tabelManajemen" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id</th>
              <th>Penjelasan</th>
              <th>0</th>
              <th>1</th>
              <th>2</th>
              <th>3</th>
              <th>4</th>
              <th>Periode</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($manajemen_setda_view->result_array() as $manajemen): ?>
              <tr>
                <td><?php echo $manajemen["id"]; ?></td>
                <td><?php echo $manajemen["penjelasan"]; ?></td>
                <td><?php echo $manajemen["nol"]; ?></td>
                <td><?php echo $manajemen["satu"]; ?></td>
                <td><?php echo $manajemen["dua"]; ?></td>
                <td><?php echo $manajemen["tiga"]; ?></td>
                <td><?php echo $manajemen["empat"]; ?></td>
                <td><?php echo $manajemen["periode"]; ?></td>
                <td><?php
                  if ($manajemen['status']=='0') {
                    echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                  } else {
                    echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                  }

                ?></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>Id</th>
                <th>Penjelasan</th>
                <th>Lancar</th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>Periode</th>
                <th>Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#tabelManajemen").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
