<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

<?php foreach ($neraca->result() as $row) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit Data Neraca</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/edit_neraca_ac'); ?>" method="post">
            <div class="col-md-4">
              <input type="hidden" name="id" value="<?php echo $row->id; ?>">
              <div class="form-group">
                <label>No. Pos</label>
                <input type="text" name="nopos" value="<?php echo $row->nopos; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Nama Pos</label>
                <input type="text" name="nmpos" value="<?php echo $row->nmpos; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Saldo Bulan Kemarin</label>
                <input type="text" name="saldoBK" value="<?php echo $row->saldoBK; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Debet</label>
                <input type="text" name="debet" value="<?php echo $row->debet; ?>" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Kredit</label>
                <input type="text" name="kredit" value="<?php echo $row->kredit; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Saldo Bulan Ini</label>
                <input type="text" name="saldo" value="<?php echo $row->saldo; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Periode</label>
                <input type="text" name="periode" id="periode" value="<?php echo $row->Periode; ?>" class="form-control">
              </div>
              <div class="form-group" align="right">
                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Simpan">
              </div>
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
