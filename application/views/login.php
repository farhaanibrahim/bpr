<!DOCTYPE HTML>
<?php foreach ($instansi->result() as $instansi) {} ?>
<html class="full">

<head>
    <title>Login</title>

    <link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/login'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/login'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/login'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/login'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/login'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('assets/login'); ?>/js/modernizr.js"></script>


</head>

<body class="full">

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">

        <div class="full-page">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-img" style="background-image:url(<?php echo base_url('assets'); ?>/wallpaper.jpg);"></div>
                <div class="bg-holder-content full text-white">
                    <a class="logo-holder" href="<?php echo base_url('login'); ?>">
                      <img src="<?php echo base_url('upload'); ?>/<?php echo $instansi->logo; ?>" style="width:10%;" alt="Image Alternative text" title="Image Title" />

                    </a>
                    <h3 style="padding-top:50px;padding-left:160px;">Sistem Informasi Monitoring & Evaluasi BUMD Bank Pasar</h3>
                    <div class="full-center">
                        <div class="container">
                            <div class="row row-wrap" data-gutter="60">
                                <div class="col-md-6">
                                    <div class="visible-lg">
                                        <h3 class="mb15">Selamat Datang</h3>
                                        <p><?php echo $instansi->tentang_singkat; ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mb15">Login</h3>
                                    <form action="<?php echo base_url('login'); ?>" method="post">
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i>
                                            <label>Username</label>
                                            <input class="form-control" name="username" placeholder="Username" type="text" />
                                        </div>
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
                                            <label>Password</label>
                                            <input class="form-control" name="password" type="password" placeholder="Password" />
                                        </div>
                                        <input class="btn btn-primary" name="btnSubmit" type="submit" value="Sign in" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="footer-links">
                        <li>
                          <strong>Copyright © 2017 SETDA Perekonomian Kota Bogor.</strong>
                        </li>
                    </ul>
                </div>
            </div>
        </div>



        <script src="<?php echo base_url('assets/login'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('assets/login'); ?>/js/custom.js"></script>
    </div>
</body>

</html>
