<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data Labarugi</h3>
        </div>


        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <div class="col-md-12">
            <div class="col-md-4">
              <form action="<?php echo base_url('app/view_labarugi_triwulan'); ?>" method="post">
                <label>Periode</label>
                <div class="input-group input-group-sm">
                <input type="text" id="periode" name="periode" class="form-control">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-flat">Go!</button>
                    </span>
              </div>
              </form>
            </div>
            <br><br><br>
            <hr>
          </div>
          
          <table id="tabelLabarugi" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id</th>
              <th>No. Pos</th>
              <th>Nama Pos</th>
              <th>Saldo Bulan Kemarin</th>
              <th>Debet</th>
              <th>Kredit</th>
              <th>Saldo Bulan Ini</th>
              <th>Periode</th>
              <?php
              if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                ?>
                <th>Status</th>
                <?php
              }
              ?>
              <?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <th id="opsi">Opsi</th>
				  <?php
			  }
			  ?>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($labarugi->result_array() as $labarugi): ?>
                <tr>
                  <td><?php echo $labarugi['id']; ?></td>
                  <td><?php echo $labarugi['nopos']; ?></td>
                  <td><?php echo $labarugi['nmpos']; ?></td>
                  <td><?php
                  if ($labarugi['saldoBK'] == " ") {
                    echo " ";
                  } else {
                    echo number_format($labarugi['saldoBK'],0,",",".");
                  }
                   ?></td>
                  <td><?php
                  if ($labarugi['debet'] == " ") {
                    echo " ";
                  } else {
                    echo number_format($labarugi['debet'],0,",",".");
                  }
                   ?></td>
                  <td><?php
                  if ($labarugi['kredit'] == " ") {
                    echo " ";
                  } else {
                    echo number_format($labarugi['kredit'],0,",",".");
                  }
                   ?></td>
                  <td><?php
                  if ($labarugi['saldo']) {
                    echo number_format($labarugi['saldo'],0,",",".");
                  }
                   ?></td>
                  <td><?php echo $labarugi['Periode']; ?></td>
                  <?php
                  if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                    ?>
                    <td><?php
                      if ($labarugi['status']=='0') {
                        echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                      } else {
                        echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                      }

                    ?></td>
                    <?php
                  }
                  ?>
				  <?php
				  if($this->session->userdata('level')!=='super_admin')
				  {
					  ?>
					  <td id="opsi">
						<?php if ($this->session->userdata('level')=='petugas'||($this->session->userdata('level')=='super_admin')): ?>
						  <a href="<?php echo base_url('app/edit_labarugi'); ?>/<?php echo $labarugi['id']; ?>" class="btn btn-warning">Edit</a>
						<?php endif; ?>
					  </td>
					  <?php
				  }
				  ?>
                  
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
              <th>Id</th>
              <th>No. Pos</th>
              <th>Nama Pos</th>
              <th>Saldo Bulan Kemarin</th>
              <th>Debet</th>
              <th>Kredit</th>
              <th>Saldo</th>
              <th>Periode</th>
              <?php
              if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                ?>
                <th>Status</th>
                <?php
              }
              ?>
              <?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <th id="opsi">Opsi</th>
				  <?php
			  }
			  ?>
            </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#tabelRekap").DataTable();
    $("#tabelNeraca").DataTable();
    $('#tabelLabarugi').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });

  $(function () {
    $("#periode").datepicker({
      format: 'yyyy-mm',
      viewMode: "months",
      minViewMode: "months",
    });
  });
</script>
