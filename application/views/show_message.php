<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<?php foreach ($msg_content->result() as $msg) {} ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Message
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>From : <?php echo $msg->dari; ?></strong></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label><?php echo $msg->dari; ?></label>
                <p><?php echo $msg->tgl; ?></p>
              </div>
              <div class="form-group">
                <label>Message</label>
                <p><?php echo $msg->pesan; ?></p>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once'template/footer.php'; ?>

