<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data Manajemen</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tabelManajemen" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id</th>
              <th>Penjelasan</th>
              <th>0</th>
              <th>1</th>
              <th>2</th>
              <th>3</th>
              <th>4</th>
              <th>Periode</th>
              <?php
              if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                ?>
                <th>Status</th>
                <?php
              }
              ?>
              <?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <th id="opsi">Opsi</th>
				  <?php
			  }
			  ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($manajemen->result_array() as $manajemen): ?>
              <tr>
                <td><?php echo $manajemen["id"]; ?></td>
                <td><?php echo $manajemen["penjelasan"]; ?></td>
                <td><?php echo $manajemen["nol"]; ?></td>
                <td><?php echo $manajemen["satu"]; ?></td>
                <td><?php echo $manajemen["dua"]; ?></td>
                <td><?php echo $manajemen["tiga"]; ?></td>
                <td><?php echo $manajemen["empat"]; ?></td>
                <td><?php echo $manajemen["periode"]; ?></td>
                <?php
                if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                  ?>
                  <td><?php
                    if ($manajemen['status']=='0') {
                      echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                    } else {
                      echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                    }

                  ?></td>
                  <?php
                }
                ?>
				<?php
			  if($this->session->userdata('level')!=='super_admin')
			  {
				  ?>
				  <td id="opsi">
                  <?php if ($this->session->userdata('level')=='petugas'||($this->session->userdata('level')=='super_admin')): ?>
                    <a href="<?php echo base_url('app/edit_manajemen'); ?>/<?php echo $manajemen['id']; ?>" class="btn btn-warning">Edit</a>
                  <?php endif; ?>
                </td>
				  <?php
			  }
			  ?>
                
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>Id</th>
                <th>Penjelasan</th>
                <th>Lancar</th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>Periode</th>
                <?php
                if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                  ?>
                  <th>Status</th>
                  <?php
                }
                ?>
                <?php
				  if($this->session->userdata('level')!=='super_admin')
				  {
					  ?>
					  <th id="opsi">Opsi</th>
					  <?php
				  }
				  ?>
              </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#tabelManajemen").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
