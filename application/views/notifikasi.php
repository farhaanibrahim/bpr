<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker'); ?>/datepicker3.css">

    <title></title>
  </head>
  <body>

    <script src="<?php echo base_url('assets/bootstrap/js'); ?>/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/datepicker'); ?>/bootstrap-datepicker.js"></script>
  </body>
</html>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
