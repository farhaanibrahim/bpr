<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left info">
        <p><?php echo $this->session->userdata('nama'); ?></p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
      <br><br>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header">MENU</li>
      <!-- Optionally, you can add icons to the links -->
      <li class="active"><a href="<?php echo base_url('app'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
      <?php if ($this->session->userdata('level')=='petugas'): ?>
        <li><a href="<?php echo base_url('app/inbox'); ?>"><i class="fa fa-envelope"></i> <span>Inbox</span></a></li>
      <?php endif ?>
      <li class="treeview" id="view_data">
        <a href="#"><i class="fa fa-th-list"></i> <span>View Data</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('app/rekap'); ?>">Rekap</a></li>
          <li><a href="<?php echo base_url('app/neraca'); ?>">Neraca</a></li>
          <li><a href="<?php echo base_url('app/labarugi'); ?>">Labarugi</a></li>
        </ul>
      </li>
      <?php if ($this->session->userdata('level')=='pimpinan'): ?>
        <li id="konfirmasi">
          <a href="<?php echo base_url('app/konfirmasi'); ?>"><i class="fa fa-check"></i><span>Konfirmasi</span></a>
        </li>
      <?php endif ?>
      <?php if ($this->session->userdata('level')=='petugas'): ?>
        <li class="treeview" id="import_data">
        <a href="#"><i class="fa fa-file"></i> <span>Import</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('app/import_rekap'); ?>">Rekap</a></li>
          <li><a href="<?php echo base_url('app/import_neraca'); ?>">Neraca</a></li>
          <li><a href="<?php echo base_url('app/import_labarugi'); ?>">Labarugi</a></li>
          <li><a href="<?php echo base_url('app/import_car'); ?>">CAR</a></li>
          <li><a href="<?php echo base_url('app/import_kap'); ?>">KAP</a></li>
          <li><a href="<?php echo base_url('app/import_ppap'); ?>">PPAP</a></li>
          <li><a href="<?php echo base_url('app/import_manajemen'); ?>">Manajemen</a></li>
          <li><a href="<?php echo base_url('app/import_roa'); ?>">ROA</a></li>
          <li><a href="<?php echo base_url('app/import_bopo'); ?>">BOPO</a></li>
          <li><a href="<?php echo base_url('app/import_cr_ldr'); ?>">CR & LDR</a></li>
        </ul>
      </li>
      <?php endif ?>
      <?php if ($this->session->userdata('level')=='petugas'): ?>
        <li id="undo_import"><a href="<?php echo base_url('app/undo_import'); ?>"><i class="fa fa-undo"></i> <span>Undo Import</span></a></li>
      <?php endif ?>
      <?php if ($this->session->userdata('level')=='super_admin'): ?>
        <li id="set_web"><a href="<?php echo base_url('app/setting_website'); ?>"><i class="glyphicon glyphicon-cog"></i> <span>Setting Website</span></a></li>
        <li id="mng_usr"><a href="<?php echo base_url('app/manage_user'); ?>"><i class="fa fa-user"></i> <span>Manajemen Pengguna</span></a></li>
      <?php endif ?>
      <?php if ($this->session->userdata('level')!='super_admin'): ?>
        <li><a href="<?php echo base_url('app/edit_account2'); ?>/<?php echo $this->session->userdata('id_user'); ?>"><i class="fa fa-pencil"></i> <span>Edit Akun</span></a></li>
      <?php endif ?>
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
