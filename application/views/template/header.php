<!DOCTYPE html>
<?php
  foreach ($instansi->result() as $instansi) {}
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
  <title>BUMD Bank Pasar</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css'); ?>/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables'); ?>/dataTables.bootstrap.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins'); ?>/skin-yellow.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker'); ?>/datepicker3.css">

  <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css"> 

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-yellow sidebar-mini">
  <style media="screen">
  .badge-notify{
     background:red;
     position: relative;
     top: -10px;
     left: -5px;
    }
  </style>
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <div class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url('upload'); ?>/<?php echo $instansi->logo; ?>" style="width:60%;" alt=""></span>
      <!-- logo for regular state and mobile devices -->
      <img src="<?php echo base_url('upload'); ?>/<?php echo $instansi->logo; ?>" style="width:21%;" alt="">
    </div>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span><strong><?php echo $instansi->nama; ?></strong>
      </a>

      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <i class="glyphicon glyphicon-user"></i><span class="hidden-xs"><?php echo $this->session->userdata('nama'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">

                <p>
                  <?php echo $this->session->userdata('nama'); ?>
                </p>
                <img src="<?php echo base_url('upload'); ?>/<?php echo $this->session->userdata('logo'); ?>" style="width:35%;" alt="">
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?php echo base_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>

          <!-- Control Sidebar Toggle Button -->
          <?php if ($this->session->userdata('level')=='petugas'): ?>
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>
                <?php if ($notif_petugas->num_rows() > 0): ?>
                  <span class="badge badge-notify"><?php echo $notif_petugas->num_rows(); ?></span>
                <?php endif; ?>
              </a>
              <ul class="dropdown-menu">
              <li class="header">You have <?php echo $notif_petugas->num_rows(); ?> messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <?php foreach ($notif_petugas->result() as $notif): ?>
                    <li><!-- start message -->
                      <a href="<?php echo base_url('app/show_message'); ?>/<?php echo $notif->id_notif; ?>">
                        <h4>
                          <?php echo $notif->dari; ?>
                          <small><i class="fa fa-clock-o"></i> <?php echo $notif->tgl; ?></small>
                        </h4>
                        <p><?php echo substr($notif->pesan,'0','35'); ?> ... </p>
                      </a>
                    </li>
                    <!-- end message -->
                  <?php endforeach; ?>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
            </li>
          <?php endif; ?>
          <li>
            <a href="#"><?php date('l, d-m-Y'); ?><div id="clock"></div></a>
            <script type="text/javascript">
				<!--
				function startTime() {
					var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
					var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
					var date = new Date();
					var day = date.getDate();
					var month = date.getMonth();
					var thisDay = date.getDay(),
					    thisDay = myDays[thisDay];
					var yy = date.getYear();
					var year = (yy < 1000) ? yy + 1900 : yy;

				    var today=new Date(),
				        curr_hour=today.getHours(),
				        curr_min=today.getMinutes(),
				        curr_sec=today.getSeconds();
				 curr_hour=checkTime(curr_hour);
				    curr_min=checkTime(curr_min);
				    curr_sec=checkTime(curr_sec);
				    document.getElementById('clock').innerHTML=thisDay+" , "+day+" "+months[month]+" "+year+" "+curr_hour+":"+curr_min+":"+curr_sec;
				}
				function checkTime(i) {
				    if (i<10) {
				        i="0" + i;
				    }
				    return i;
				}
				setInterval(startTime, 500);
				//-->
				</script>
          </li>
        </ul>
      </div>
    </nav>
  </header>
