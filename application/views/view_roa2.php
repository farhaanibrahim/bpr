<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data Return On Asset</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tabelROA" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id</th>
              <th>Bulan</th>
              <th>Volume Usaha</th>
              <th>Laba Th Jalan</th>
              <th>Net Laba</th>
              <th>Periode</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($roa_setda_view->result_array() as $roa): ?>
              <tr>
                <td><?php echo $roa["id"]; ?></td>
                <td><?php echo $roa["bulan"]; ?></td>
                <td><?php echo $roa["vol_usaha"]; ?></td>
                <td><?php echo $roa["laba_th_jalan"]; ?></td>
                <td><?php echo $roa["net_laba"]; ?></td>
                <td><?php echo $roa["periode"]; ?></td>
                <td><?php
                  if ($roa['status']=='0') {
                    echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                  } else {
                    echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                  }

                ?></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>Id</th>
                <th>Bulan</th>
                <th>Volume Usaha</th>
                <th>Laba Th Jalan</th>
                <th>Net Laba</th>
                <th>Periode</th>
                <th>Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#tabelROA").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
