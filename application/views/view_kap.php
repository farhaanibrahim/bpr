<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Data Kapital Aktiva Produktif</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->session->flashdata('edit_sukses'); ?>
          <table id="tableKap" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th rowspan="2">Id</th>
                <th rowspan="2">KETERANGAN</th>
                <th colspan="3" class="text-center">Posisi Bulan Lalu</th>
                <th colspan="3" class="text-center">Posisi Saat Ini</th>
                <th rowspan="2">Periode</th>
                <?php
                if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                  ?>
                  <th rowspan="2">Status</th>
                  <?php
                }
                ?>
				<?php
				  if($this->session->userdata('level')!=='super_admin')
				  {
					  ?>
					  <th rowspan="2" id="opsi"></th>
					  <?php
				  }
				  ?>
              </tr>
            <tr>
              <th>Kredit</th>
              <th>BI+ABA non Giro</th>
              <th>Jumlah</th>
              <th>Kredit</th>
              <th>BI+ABA non Giro</th>
              <th>Jumlah</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($kap->result_array() as $kap): ?>
              <tr>
                <td><?php echo $kap["id"]; ?></td>
                <td><?php echo $kap["keterangan"]; ?></td>
                <td><?php echo $kap["kredit_bln_lalu"]; ?></td>
                <td><?php echo $kap["bi_aba_non_giro_bln_lalu"]; ?></td>
                <td><?php echo $kap["jml_bln_lalu"]; ?></td>
                <td><?php echo $kap["kredit_saat_ini"]; ?></td>
                <td><?php echo $kap["bi_aba_non_giro_saat_ini"]; ?></td>
                <td><?php echo $kap["jml_saat_ini"]; ?></td>
                <td><?php echo $kap["periode"]; ?></td>
                <?php
                if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                  ?>
                  <td><?php
                    if ($kap['status']=='0') {
                      echo "<span class='label label-danger'>Belum dikonfirmasi</span>";
                    } else {
                      echo "<span class='label label-success'>Sudah dikonfirmasi</span>";
                    }

                  ?></td>
                  <?php
                }
                ?>
				<?php
				  if($this->session->userdata('level')!=='super_admin')
				  {
					  ?>
					  <td id="opsi">
					  <?php if ($this->session->userdata('level')=='petugas'||($this->session->userdata('level')=='super_admin')): ?>
						<a href="<?php echo base_url('app/edit_kap'); ?>/<?php echo $kap['id']; ?>" class="btn btn-warning">Edit</a>
					  <?php endif; ?>
					</td>
					  <?php
				  }
				  ?>
                
              </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <td rowspan="2">Id</td>
                <td rowspan="2">KETERANGAN</td>
                <td>Kredit</td>
                <td>BI_ABA non Giro</td>
                <td>Jumlah</td>
                <td>Kredit</td>
                <td>BI_ABA non Giro</td>
                <td>Jumlah</td>
                <td rowspan="2">Periode</td>
                <?php
                if (($this->session->userdata('level')=='petugas')||($this->session->userdata('level')=='pimpinan')||($this->session->userdata('level')=='super_admin')) {
                  ?>
                  <th rowspan="2">Status</th>
                  <?php
                }
                ?>
				<?php
				  if($this->session->userdata('level')!=='super_admin')
				  {
					  ?>
					  <td rowspan="2" id="opsi"></td>
					  <?php
				  }
				  ?>
                
              </tr>
              <tr>
                <td colspan="3">Posisi Bulan Lalu</td>
                <td colspan="3">Posisi Saat Ini</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#tableKap").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
