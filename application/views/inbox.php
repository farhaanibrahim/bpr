<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Inbox
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>From</th>
                    <th>Message</th>
                    <th>Date</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($inbox->result() as $inbox): ?>
                    <?php $no = 1; ?>
                    <tr style="<?php if ($inbox->status == 'unread') {
                        ?>
                        background: #f4f4f4;
                        <?php
                      } ?>
                    ">
                      <td><?php echo $no; ?></td>
                      <td><?php echo $inbox->dari; ?></td>
                      <td><?php echo $inbox->pesan; ?></td>
                      <td><?php echo $inbox->tgl; ?></td>
                      <td><a href="<?php echo base_url('app/show_message'); ?>/<?php echo $inbox->id_notif; ?>" class="btn btn-primary">Read</a></td>
                    </tr>
                    <?php $no++; ?>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once'template/footer.php'; ?>

