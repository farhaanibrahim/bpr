<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>

<?php foreach ($ppap->result() as $row) {}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid box-primary">
      <div class="box-body">
        <div class="box-header">
          <h3 class="box-title">Edit Data PPAP</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="" action="<?php echo base_url('app/edit_ppap_ac'); ?>" method="post">
            <div class="col-md-4">
              <input type="hidden" name="id" value="<?php echo $row->id; ?>">
              <div class="form-group">
                <label>Keterangan</label>
                <input type="text" name="keterangan" value="<?php echo $row->keterangan; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Lancar</label>
                <input type="text" name="lancar" value="<?php echo $row->lancar; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Kurang Lancar</label>
                <input type="text" name="kurang_lancar" value="<?php echo $row->kurang_lancar; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Diragukan</label>
                <input type="text" name="diragukan" value="<?php echo $row->diragukan; ?>" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Macet</label>
                <input type="text" name="macet" value="<?php echo $row->macet; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Jumlah</label>
                <input type="text" name="jumlah" value="<?php echo $row->jumlah; ?>" class="form-control">
              </div>
              <div class="form-group">
                <label>Periode</label>
                <input type="text" name="periode" id="periode" value="<?php echo $row->periode; ?>" class="form-control">
              </div>
              <div class="form-group" align="right">
                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Simpan">
              </div>
            </div>
          </form>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
