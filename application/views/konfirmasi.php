<?php
  include_once'template/header.php';
  include_once'template/side.php';
?>
<style media="screen">
  #myModal1 .modal-dialog  {
    width:75%;
  }
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Konfirmasi
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Konfirmasi</h3>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="box-body">
            <?php echo $this->session->flashdata('konfirmasi_sukses'); ?>
            <?php echo $this->session->flashdata('notifikasi_sukses'); ?>
            <div class="alert alert-info alert-dismissable text-center"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4>Apakah data sudah benar?</h4></div>

            <div class="col-md-4">
              <form class="" action="<?php echo base_url('app/konfirmasi_ac'); ?>" method="post">
			    <div class="form-group">
					<label>File</label>
					<select class="form-control" name="table" required>
						<option>-- Pilih File --</option>
						<option value="t_rekap">Rekap</option>
						<option value="neraca">Neraca</option>
						<option value="labarugi">Labarugi</option>
						<option value="t_car">CAR</option>
						<option value="t_kap">KAP</option>
						<option value="t_ppap">PPAP</option>
						<option value="t_manajemen">Management</option>
						<option value="t_roa">ROA</option>
						<option value="t_bopo">BOPO</option>
						<option value="t_cr_ldr">CR & LDR</option>
					</select>
				</div>
                <div class="form-group">
                  <label for="">Periode</label>
                  <input type="text" name="periode" id="periode" placeholder="Periode" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="">Keterangan</label><small>(Isi form ini jika ingin memberikan notifikasi)</small>
                  <textarea name="ket" class="form-control"></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="konfirmasi" value="Konfirmasi" class="btn btn-success">
                  <input type="submit" name="notifikasi" value="Notifikasi" class="btn btn-danger">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>


<?php include_once'template/footer.php'; ?>
<script>
  $(function () {
    $("#periode").datepicker({
  		format: 'yyyy-mm',
  		viewMode: "months",
  		minViewMode: "months",
  	});
  });
</script>
